/*
Navicat SQL Server Data Transfer

Source Server         : SQL server
Source Server Version : 105000
Source Host           : localhost:1433
Source Database       : QQ_DateBase
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 105000
File Encoding         : 65001

Date: 2020-08-20 15:35:20
*/


-- ----------------------------
-- Table structure for QQfriend
-- ----------------------------
DROP TABLE [dbo].[QQfriend]
GO
CREATE TABLE [dbo].[QQfriend] (
[qqid] int NOT NULL ,
[friend] int NOT NULL 
)


GO

-- ----------------------------
-- Records of QQfriend
-- ----------------------------
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10005', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10010', N'222222')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10011', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10013', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10014', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10015', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10016', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'10017', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10005')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10011')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10013')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10014')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10015')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10016')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'10017')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'54321')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'98765')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'12345', N'111111')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'54321', N'10010')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'54321', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'54321', N'98765')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'54321', N'111111')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'98765', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'98765', N'54321')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'98765', N'444444')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'111111', N'12345')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'111111', N'54321')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'222222', N'10010')
GO
GO
INSERT INTO [dbo].[QQfriend] ([qqid], [friend]) VALUES (N'444444', N'98765')
GO
GO

-- ----------------------------
-- Table structure for QQmessage
-- ----------------------------
DROP TABLE [dbo].[QQmessage]
GO
CREATE TABLE [dbo].[QQmessage] (
[mid] int NULL ,
[micon] int NULL ,
[mname] char(20) NULL 
)


GO

-- ----------------------------
-- Records of QQmessage
-- ----------------------------
INSERT INTO [dbo].[QQmessage] ([mid], [micon], [mname]) VALUES (N'9999', N'8', N'腾讯新闻            ')
GO
GO

-- ----------------------------
-- Table structure for QQpersoninfo
-- ----------------------------
DROP TABLE [dbo].[QQpersoninfo]
GO
CREATE TABLE [dbo].[QQpersoninfo] (
[qqid] int NOT NULL ,
[qqpassword] int NULL ,
[qqqianming] char(50) NULL ,
[qqicon] smallint NULL ,
[qqname] varchar(50) NULL 
)


GO

-- ----------------------------
-- Records of QQpersoninfo
-- ----------------------------
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10001', N'10001', N'宝宝没想好                                        ', N'6', N'老亚瑟')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10005', N'10005', N'随便来一句                                        ', N'5', N'狄仁杰')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10008', N'10008', N'这是签名哦哦哦                                    ', N'3', N'凶悍的小奶狗')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10010', N'10010', N'我是电信                                          ', N'5', N'电信10010')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10011', N'10011', N'坦克无敌                                          ', N'6', N'庄周')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10012', N'10012', N'攻城狮就是我我就是攻城狮                          ', N'3', N'工程师')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10013', N'10013', N'一二三四，一二三四，像首歌                        ', N'2', N'基尔')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10014', N'10014', N'我是夏未央                                        ', N'1', N'未央')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10015', N'10015', N'启程未来启迪智慧                                  ', N'2', N'启程')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10016', N'10016', N'启程未来启迪智慧                                  ', N'2', N'启程2')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'10017', N'10017', N'启程未来启迪智慧33                                ', N'3', N'启程3')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'12345', N'12345', N'情不知所起，一往而深！                            ', N'1', N'西岸风')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'54321', N'54321', N'我喜欢你，就像数学里的有且仅有你                  ', N'6', N'夏微凉')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'98765', N'98765', N'早起早睡，做一个健康的帅逼                        ', N'3', N'繁华')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'166001', N'166001', N'本宝宝还没想到好的签名                            ', N'7', N'一')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'222222', N'222222', N'本宝宝还没想到好的签名                            ', N'2', N'全是二')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'333333', N'333333', N'本宝宝还没想到好的签名                            ', N'2', N'全是三')
GO
GO
INSERT INTO [dbo].[QQpersoninfo] ([qqid], [qqpassword], [qqqianming], [qqicon], [qqname]) VALUES (N'444444', N'444444', N'本宝宝还没想到好的签名                            ', N'2', N'全是四')
GO
GO

-- ----------------------------
-- Table structure for QQquninfo
-- ----------------------------
DROP TABLE [dbo].[QQquninfo]
GO
CREATE TABLE [dbo].[QQquninfo] (
[qid] int NOT NULL ,
[qgonggao] varchar(100) NULL ,
[qname] varchar(10) NOT NULL ,
[qowner] int NOT NULL ,
[qtx] int NOT NULL 
)


GO

-- ----------------------------
-- Records of QQquninfo
-- ----------------------------
INSERT INTO [dbo].[QQquninfo] ([qid], [qgonggao], [qname], [qowner], [qtx]) VALUES (N'1998', N'一起爽翻天', N'天上人间', N'12345', N'9')
GO
GO
INSERT INTO [dbo].[QQquninfo] ([qid], [qgonggao], [qname], [qowner], [qtx]) VALUES (N'1999', N'暂无公告', N'牛逼克拉斯', N'12345', N'6')
GO
GO
INSERT INTO [dbo].[QQquninfo] ([qid], [qgonggao], [qname], [qowner], [qtx]) VALUES (N'2000', N'本宝宝不想写群公告！！！', N'你好世界', N'12345', N'9')
GO
GO

-- ----------------------------
-- Table structure for QQqunmember
-- ----------------------------
DROP TABLE [dbo].[QQqunmember]
GO
CREATE TABLE [dbo].[QQqunmember] (
[qid] int NOT NULL ,
[qmember] int NOT NULL 
)


GO

-- ----------------------------
-- Records of QQqunmember
-- ----------------------------
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'10010')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'12345')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'54321')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'98765')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'111111')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'222222')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1998', N'444444')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1999', N'166')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1999', N'12345')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'1999', N'54321')
GO
GO
INSERT INTO [dbo].[QQqunmember] ([qid], [qmember]) VALUES (N'2000', N'12345')
GO
GO

-- ----------------------------
-- Indexes structure for table QQfriend
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QQfriend
-- ----------------------------
ALTER TABLE [dbo].[QQfriend] ADD PRIMARY KEY ([qqid], [friend])
GO

-- ----------------------------
-- Indexes structure for table QQpersoninfo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QQpersoninfo
-- ----------------------------
ALTER TABLE [dbo].[QQpersoninfo] ADD PRIMARY KEY ([qqid])
GO

-- ----------------------------
-- Indexes structure for table QQquninfo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QQquninfo
-- ----------------------------
ALTER TABLE [dbo].[QQquninfo] ADD PRIMARY KEY ([qid])
GO

-- ----------------------------
-- Indexes structure for table QQqunmember
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QQqunmember
-- ----------------------------
ALTER TABLE [dbo].[QQqunmember] ADD PRIMARY KEY ([qid], [qmember])
GO
