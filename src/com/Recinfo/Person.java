package com.Recinfo;

public class Person{
	
	private String name;
	private String qq;
	private String touxiang;
	
  
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getQq() {
		return qq;
	}


	public void setQq(String qq) {
		this.qq = qq;
	}


	public String getTouxiang() {
		return touxiang;
	}


	public void setTouxiang(String touxiang) {
		this.touxiang = touxiang;
	}


	public Person(String name, String qq, String touxiang) {
		this.name = name;
		this.qq = qq;
		this.touxiang = touxiang;
	}


}
