package com.Recinfo;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.Socket;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UIManager;

import com.Recinfo.QQ_RecinfoThread;




public class MySystemTray implements ActionListener{




    private TrayIcon trayIcon = null; // 托盘图标
    private Timer shanshuo = null;
    private ImageIcon icon1 = null;
    private ImageIcon icon2 = null;
    private SystemTray tray = null; // 本操作系统托盘的实例
    boolean gengai = false;
    String name="";
    String qq="";
    JFrame jf=null;
    QQ_NoReadMess noreadmess;
    String logininfo;
    Socket socket;
    
    //采用jdk1.6的托盘技术 实现跨平台的应用
    
    
    
    public MySystemTray(String name,String qq,String myicon,JFrame jf,Socket socket,String logininfo) {
    	this.jf=jf;
    	this.name=name;
    	this.qq=qq;
    	this.logininfo=logininfo;
    	this.socket=socket;
    
    	//消息提示
    	noreadmess = new QQ_NoReadMess(socket,new Person(name,qq,myicon),logininfo);
    	
    	
    	//启动接受消息线程
  		QQ_RecinfoThread rec=new QQ_RecinfoThread(socket,noreadmess);
  		
        icon1 = new ImageIcon(".\\icon\\tray1.gif"); // 将要显示到托盘中的图标
        icon2 = new ImageIcon(".\\icon\\tray2.jpg"); // 将要显示到托盘中的图标
        try {
            // 将LookAndFeel设置成Windows样式
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //判断是否支持托盘
        if (SystemTray.isSupported()) {
            this.tray();
        }
        shanshuo = new Timer(600,this);
        shanshuo(false);
        
        new Thread(new Runnable() {

			@Override
			public void run() {
				while(true) {
						
						try {
							shanshuo(!noreadmess.getRecmess().isEmpty());
							Thread.currentThread().sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
				}
				
			}
        	
        }) {}.start();
    }
    
    //控制图标闪烁
    public void shanshuo(boolean flag) {
    	if(flag==true) {
    		trayIcon.setToolTip("");
    		shanshuo.start();}
    	else {
    		trayIcon.setToolTip(settrayinfo());
    		shanshuo.stop();
    		trayIcon.setImage(icon1.getImage());	
    	}
    	
    }

    

    //显示提示
    public String settrayinfo() {
    	String line1="QQ: "+this.name+" ("+this.qq+")"+"\n";
    	String line2="声音：开启"+"\n";
    	String line3="消息提醒框：开启"+"\n";
    	String line4="会话消息：任务栏头像闪动";
    	return line1+line2+line3+line4;
    }

    /**
     * 托盘相关代码
     */
    private void tray() {
    
   
        tray = SystemTray.getSystemTray(); // 获得本操作系统托盘的实例
        PopupMenu pop = new PopupMenu(); // 构造一个右键弹出式菜单
        MenuItem show = new MenuItem("主界面");
        MenuItem author = new MenuItem("关于");  
        MenuItem exit = new MenuItem("退出");
        trayIcon = new TrayIcon(icon1.getImage(), settrayinfo(), pop);

        /**
         * 添加鼠标监听器，当鼠标在托盘图标上双击时，默认显示窗口
         */
        trayIcon.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() ==2&&e.getButton()==e.BUTTON1) { // 鼠标左键单击
                 
                    jf.setVisible(true); // 显示窗口
                }
            }
        });
        mousejiantin jian=new mousejiantin(noreadmess);
        trayIcon.addMouseListener(jian);
        
        
        
        show.addActionListener(new ActionListener() { // 点击“显示窗口”菜单后将窗口显示出来

            public void actionPerformed(ActionEvent e) {
               
                jf.setVisible(true); // 显示窗口
            }
        });
        exit.addActionListener(new ActionListener() { // 点击“退出演示”菜单后推出程序

            public void actionPerformed(ActionEvent e) {
        
                System.exit(0); // 退出程序
            }
        });
        author.addActionListener(new ActionListener() { // 点击“退出演示”菜单后推出程序

            public void actionPerformed(ActionEvent e) {
                showMessage();
            }
        });
        pop.add(show);
        pop.add(author);
        pop.add(exit);
    }
    public TrayIcon getTrayIcon() {
		return trayIcon;
	}

	/**
     * 显示信息
     */
    private void showMessage() {
        JOptionPane.showMessageDialog(jf, "版本     v1.7 ", "关于", JOptionPane.INFORMATION_MESSAGE);
    }
    
    
 public void yinca(){
      try {
   tray.add(trayIcon);
  } catch (AWTException e) {
  
   e.printStackTrace();
  } // 将托盘图标添加到系统的托盘实例中

    }
 @Override
 public void actionPerformed(ActionEvent arg0) {
  if(!gengai){
   trayIcon.setImage(icon2.getImage());
   gengai = true;
  }else{
   trayIcon.setImage(icon1.getImage());
   gengai = false;
  }
 }
}


class mousejiantin implements MouseListener{
	
	
	QQ_NoReadMess noreadmess;
	
	mousejiantin(QQ_NoReadMess noreadmess){
		this.noreadmess=noreadmess;}

	@Override
	public void mouseClicked(MouseEvent e) {
		 if (e.getClickCount() ==1&&e.getButton()==e.BUTTON1) { // 鼠标左键单击
			 if(noreadmess.isVisible())
		         noreadmess.setVisible(false);
			 else
				 noreadmess.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
