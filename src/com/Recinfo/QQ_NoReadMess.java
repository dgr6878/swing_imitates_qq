package com.Recinfo;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.view.QQ_ChatFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class QQ_NoReadMess extends JFrame {

	private JPanel contentPane;
	private JScrollPane js;
	String logininfo;
    private JLabel name;
    private JPanel p;
    public static int labelheight=26;
    public static int labelwidth=243;
    int personnum=0;
    int labeljiange=2;
    Socket to;
    Person me;
    JFrame jf=this;
    List<Person> list=new ArrayList<Person>();
    List<String> recmess=new ArrayList<String>();
    Map<Person,String> map=new HashMap<Person,String>();
    
    
    public String addmess(String s) {
    
    	Person p=getPerson(s);
    	if(!containPerson(p)) {
    	recmess.add(s);
    	addperson(p);
    	map.put(p, s);}
    	return s;
    }
    public boolean containPerson(Person p) {
    	Iterator it=map.keySet().iterator();
    	while(it.hasNext()) {
    		Person temp=(Person) it.next();
    		if(p.getQq().equals(temp.getQq())) {
    			return true;
    		}
    	}
    	return false;
    }

	public List<String> getRecmess() {
		return recmess;
	}
    public Person getPerson(String s) {
    	String youqq=s.substring(1, s.indexOf("#",1));//#54321#12345#12
    	
    	int start=logininfo.indexOf(youqq)+youqq.length()+1;
    	int txstart=logininfo.indexOf("#",start)+1;
    	int namestart=logininfo.indexOf("#",txstart)+1;
    	int end=logininfo.indexOf("#",namestart);
    	String youname=logininfo.substring(namestart, end);
    	String youicon=logininfo.substring(txstart, namestart-1);
    	
    	return new Person(youname,youqq,youicon);
    }
	public QQ_NoReadMess(Socket to,Person me,String logininfo) {
	
	     
		this.logininfo=logininfo;
		this.to=to;
		this.me=me;
		setUndecorated(true);
		Dimension   screensize   =   Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) ((int)screensize.getWidth()*0.75);
		int height = (int) ((int)screensize.getHeight()*0.75);
		setBounds(width, height, 260, 154);//1150   630
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel topcolor = new JLabel("");
		topcolor.setBackground(new Color(0, 0, 255));
		topcolor.setBounds(0, 0, 261, 3);
		topcolor.setOpaque(true);
		contentPane.add(topcolor);
		
		name = new JLabel(me.getName());
		name.setBounds(10, 7, 116, 28);
		ImageIcon image = new ImageIcon(".\\icon\\"+me.getTouxiang()+".jpg");
		image.setImage(image.getImage().getScaledInstance(name.getHeight(), name.getHeight(),Image.SCALE_DEFAULT ));
		name.setIcon(image);
		name.setFont(new Font("黑体", Font.PLAIN, 16));
		
		contentPane.add(name);
		
		JLabel linetop = new JLabel("");
		linetop.setOpaque(true);
		linetop.setBackground(Color.LIGHT_GRAY);
		linetop.setBounds(0, 35, 275, 1);
		contentPane.add(linetop);
		
		
		
		
		p=new JPanel();
	    p.setOpaque(true);
	    p.setBackground(SystemColor.WHITE);
	    p.setLayout(null);
	    
	     js=new JScrollPane(p);
		 js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		 js.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		 js.setBounds(0, 35, 261, 91); 
		 js.getVerticalScrollBar().setUnitIncrement(10);//鼠标滑轮速度
		 p.setPreferredSize(new Dimension(261,91));
		 p.validate();
		 getContentPane().add(js);
		
		JLabel linedown = new JLabel("");
		linedown.setBackground(Color.LIGHT_GRAY);
		linedown.setOpaque(true);
		linedown.setBounds(0, 125, 275, 1);
		contentPane.add(linedown);
		
		JButton hulue = new JButton("\u5FFD\u7565\u5168\u90E8");
		hulue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//忽略全部
				p.removeAll();
				list.removeAll(list);
				recmess.removeAll(recmess);
				personnum=0;
				map.clear();
				
				p.setPreferredSize(new Dimension(261,91));
				p.validate();
				p.repaint();
				System.out.println("忽略全部，未关闭窗口");
				//setVisible(false);
				
			}
		});
		hulue.setFont(new Font("黑体", Font.PLAIN, 14));
		hulue.setContentAreaFilled(false);
		hulue.setForeground(new Color(0, 204, 255));
		hulue.setBounds(0, 129, 97, 23);
		contentPane.add(hulue);
		
		JButton hidden = new JButton("");
		hidden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		hidden.setIcon(new ImageIcon(".\\icon\\lclose.gif"));
		hidden.setBounds(221, 8, 21, 21);
		contentPane.add(hidden);
		
	    Mouse move=new Mouse();
	    addMouseListener(move);
		
	}
	public void addperson(Person per) {
		int hei;
		 JLabel person = new JLabel(per.getName());
		 person.setBounds(0, hei=personnum*(labelheight+labeljiange), labelwidth, labelheight);
		 person.setOpaque(true);
		 person.setBackground(Color.LIGHT_GRAY);
		 ImageIcon image = new ImageIcon(".\\icon\\"+per.getTouxiang()+".jpg");
		    image.setImage(image.getImage().getScaledInstance(person.getHeight(), person.getHeight(),Image.SCALE_DEFAULT ));
			person.setIcon(image);
         if((hei+labelheight+labeljiange)>js.getHeight()) {
        	 p.setPreferredSize(new Dimension(261,hei+labelheight+labeljiange));
    		 p.validate();
		 }
		 p.add(person);
		 list.add(per);
		 personnum++;
		 
		 MouseJian jian=new MouseJian(me,per,to,person);
		 person.addMouseListener(jian);
		 
		
		 
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	class MouseJian implements MouseListener{
		
		Person me;
		Person you;
		Socket to;
		JLabel l;
		public MouseJian(Person me, Person you,Socket to,JLabel l) {
			this.me = me;
			this.you = you;
			this.to=to;
			this.l=l;
		}
		public void dellabel() {
			p.removeAll();
			p.setPreferredSize(new Dimension(261,91));
			personnum=0;
			
			list.remove(you);
			List<Person> list2=new ArrayList<Person>();
			list2.addAll(list);
			list.clear();
			Iterator it=list2.iterator();
			while(it.hasNext()) {
				addperson((Person)it.next());
			}
			
			p.validate();
			p.repaint();
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			if(e.getClickCount()==1&&e.getButton()==e.BUTTON1) {
				QQ_ChatFrame chat=new QQ_ChatFrame(to,you.getQq(),me.getQq(),you.getName(),me.getTouxiang(),you.getTouxiang());
				chat.setVisible(true);
				String duifang=map.get(you);
				recmess.remove(duifang);
				map.remove(you);
				dellabel();
				//setVisible(false);
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			l.setBackground(Color.cyan);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			l.setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	class Mouse implements MouseListener{
		int x1,x2,y1,y2;

		@Override
		public void mouseClicked(MouseEvent arg0) {	
		}

		@Override
		public void mouseEntered(MouseEvent e) {	
		}

		@Override
		public void mouseExited(MouseEvent arg0) {	
		}

		@Override
		public void mousePressed(MouseEvent e) {
			x1=e.getX();
			y1=e.getY();
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			x2=e.getX();
			y2=e.getY();
			int x3=jf.getX();
			int y3=jf.getY();
	        jf.setBounds(x3+(x2-x1), y3+(y2-y1), jf.getWidth(), jf.getHeight());

			
		}
		
	  }
	}

