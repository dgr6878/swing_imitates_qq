package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;

public class QQ_xiugai extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField name;
	private JTextField icontxt;
	private JPasswordField pass;
	private JTextArea qm;
	DataOutputStream out;
	DataInputStream in;
	private JTextField myqqtxt;
	String ip;
	int port;
	JLabel passlabel;
	JButton btnenter;
	JLabel bg;
	
	
	public QQ_xiugai(int x,int y,String qq,String tx,String wm,String gx) {
		this(x,y,qq,tx,wm,gx,"mima","localhost",1234);
		setBounds(x, y, 399, 302-80);
		passlabel.setVisible(false);
		pass.setVisible(false);
		btnenter.setVisible(false);
		name.setEnabled(false);
		qm.setEnabled(false);
		icontxt.setEnabled(false);
		myqqtxt.setEnabled(false);
		bg.setIcon(new ImageIcon(".\\icon\\ziliao.jpg"));
		
	}

	public QQ_xiugai(int x,int y,final String myqq,String tx,String wm,String gx,String mima,final String ip,final int port) {
		setResizable(false);//不可改变大小
		this.ip=ip;
		this.port=port;
		setFont(new Font("黑体", Font.PLAIN, 16));
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("\u4E2A\u4EBA\u8D44\u6599");
		setBounds(x, y, 399, 302);
//		setBounds(700, 130, 399, 302);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JLabel icon = new JLabel("");
		icon.setIcon(new ImageIcon(".\\icon\\"+tx+".jpg"));
		icon.setBounds(27, 22, 50, 50);
		contentPane.add(icon);
		
		name = new JTextField();
		name.setText(wm);
		name.setBounds(153, 22, 204, 25);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("\u6635\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 16));
		lblNewLabel.setBounds(105, 22, 58, 31);
		contentPane.add(lblNewLabel);
		
		JLabel label = new JLabel("\u4E2A\u6027\u7B7E\u540D\uFF1A");
		label.setFont(new Font("黑体", Font.PLAIN, 16));
		label.setBounds(105, 63, 80, 31);
		contentPane.add(label);
		
		qm = new JTextArea();
		qm.setLineWrap(true);
		qm.setText(gx);
		qm.setBounds(178, 57, 179, 104);
		contentPane.add(qm);
		
		icontxt = new JTextField();
		icontxt.setText(tx);
		icontxt.setBounds(39, 80, 43, 21);
		contentPane.add(icontxt);
		icontxt.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("\u5934\u50CF\uFF1A");
		lblNewLabel_1.setBounds(10, 82, 43, 15);
		contentPane.add(lblNewLabel_1);
		
		passlabel = new JLabel("\u5BC6\u7801\uFF1A");
		passlabel.setFont(new Font("黑体", Font.PLAIN, 16));
		passlabel.setBounds(105, 167, 58, 31);
		contentPane.add(passlabel);
		
		btnenter = new JButton("\u4FEE  \u6539");
		btnenter.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				String tip="";
				if(name.getText().equals(""))
					tip=tip+"昵称不可以为空\n";
				if(pass.getText().equals(""))
					tip=tip+"密码不可以为空\n";
				if(icontxt.getText().equals(""))
					tip=tip+"头像不可以为空\n";
				if(qm.getText().length()>20)
					tip=tip+"个性签名太长了\n";
				for(int i=1;i<99;i++) {
					if(icontxt.getText().equals(""+i)) {
						break;
					}
					if(i==99) {
						tip=tip+"头像格式有误(键入1-99)\n";
					}
					
					}
				if(!tip.equals("")) {
					JOptionPane.showMessageDialog(qm,tip, "提示", JOptionPane.INFORMATION_MESSAGE);
				}else {
				
				
				
				
				
				try {
					@SuppressWarnings("resource")
					Socket cli=new Socket(ip,port);
					out = new DataOutputStream(cli.getOutputStream());
					in = new DataInputStream(cli.getInputStream());
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					String sql1=" update QQpersoninfo " + 
							"set qqname='"+name.getText()+"'" + 
							"  where qqid="+myqq;
					String sql2=" update QQpersoninfo " + 
							"set qqqianming='"+qm.getText()+"'" + 
							"  where qqid="+myqq;
					String sql3=" update QQpersoninfo " + 
							"set qqpassword='"+pass.getText()+"'" + 
							"  where qqid="+myqq;
					String sql4=" update QQpersoninfo " + 
							"set qqicon='"+icontxt.getText()+"'" + 
							"  where qqid="+myqq;
					
					out.writeUTF("%%%");
					out.writeUTF(sql1+sql2+sql3+sql4);
					if(in.readUTF().equals("成功")) {
						 JOptionPane.showMessageDialog(qm,"修改成功!", "提示", JOptionPane.INFORMATION_MESSAGE);
						 
					}else {
						JOptionPane.showMessageDialog(qm,"修改失败", "提示", JOptionPane.INFORMATION_MESSAGE);
					}
					//cli.close();
					setVisible(false);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}}
		});
		btnenter.setBounds(272, 216, 97, 23);
		contentPane.add(btnenter);
		
		pass = new JPasswordField();
		pass.setText(mima);
		pass.setBounds(153, 171, 210, 25);
		contentPane.add(pass);
		
		JLabel lblqq = new JLabel("   QQ\uFF1A");
		lblqq.setBounds(10, 127, 58, 15);
		contentPane.add(lblqq);
		
		myqqtxt = new JTextField();
		myqqtxt.setEditable(false);
		myqqtxt.setText(myqq);
		myqqtxt.setBounds(67, 124, 80, 21);
		contentPane.add(myqqtxt);
		myqqtxt.setColumns(10);
		
		bg = new JLabel("");
		bg.setIcon(new ImageIcon(".\\icon\\zhucebg.jpg"));
		bg.setBounds(0, 0, 436, 296);
		contentPane.add(bg);
		new Thread(new Runnable() {

			@SuppressWarnings("static-access")
			@Override
			public void run() {
				while(true) {
					try {
						Thread.currentThread().sleep(200);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(icontxt.getText().length()>0)
					icon.setIcon(new ImageIcon(".\\icon\\"+icontxt.getText()+".jpg"));
				}
				
			}}).start();
	}
	
	
	@SuppressWarnings("deprecation")
	public String[] reinfo() {
		String[] info=new String[4];
		info[0]=icontxt.getText();
		info[1]=name.getText();
		info[2]=qm.getText();
		info[3]=pass.getText();
		
		
		return info;
		
	}

}
