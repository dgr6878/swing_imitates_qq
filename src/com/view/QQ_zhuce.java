package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.baidu.ai.aip.Sample;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;

public class QQ_zhuce extends JFrame {

	/**
	 * 
	 */
	public static String FACEPATH="D:\\";
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField name;
	private JTextField qqid;
	private JPasswordField qqpass;
	private JPasswordField qqpass2;
	private JTextField touxiang;
	private JTextArea qm;
	private JButton btnenter;
	private String ip;
	private int port;
	private JTextField txtface;
	JFrame f=this;


	public QQ_zhuce(String ip,int port) {
		this.ip=ip;
		this.port=port;
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("\u6B22\u8FCE\u6CE8\u518CQQ");
		setBounds(530, 150, 450, 453);
		//setAlwaysOnTop(true);
		setResizable(false);//不可改变大小
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("\u6635 \u79F0\uFF1A");
		label.setFont(new Font("黑体", Font.PLAIN, 16));
		label.setBounds(29, 23, 64, 37);
		contentPane.add(label);
		
		name = new JTextField();
		name.setBounds(145, 32, 166, 21);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblQq = new JLabel("QQ  \u53F7\u7801\uFF1A");
		lblQq.setFont(new Font("黑体", Font.PLAIN, 16));
		lblQq.setBounds(29, 217, 96, 37);
		contentPane.add(lblQq);
		
		qqid = new JTextField();
		qqid.setColumns(10);
		qqid.setBounds(145, 226, 166, 21);
		contentPane.add(qqid);
		
		JLabel lblQq_1 = new JLabel("QQ  \u5BC6\u7801\uFF1A");
		lblQq_1.setFont(new Font("黑体", Font.PLAIN, 16));
		lblQq_1.setBounds(29, 252, 96, 37);
		contentPane.add(lblQq_1);
		
		qqpass = new JPasswordField();
		qqpass.setBounds(145, 259, 166, 21);
		contentPane.add(qqpass);
		
		JLabel label_1 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		label_1.setFont(new Font("黑体", Font.PLAIN, 16));
		label_1.setBounds(29, 287, 96, 37);
		contentPane.add(label_1);
		
		qqpass2 = new JPasswordField();
		qqpass2.setBounds(145, 296, 166, 21);
		contentPane.add(qqpass2);
		
		JLabel label_2 = new JLabel("\u4E2A\u6027\u7B7E\u540D\uFF1A");
		label_2.setFont(new Font("黑体", Font.PLAIN, 16));
		label_2.setBounds(29, 101, 83, 37);
		contentPane.add(label_2);
		
		qm = new JTextArea();
		qm.setLineWrap(true);
		qm.setBounds(145, 109, 259, 86);
		contentPane.add(qm);
		
		btnenter = new JButton("\u786E\u8BA4");
		btnenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					//zhuce  face
					if(qqid.getText().substring(0, 1).equals("0")||qqpass.getText().substring(0, 1).equals("0"))
						JOptionPane.showMessageDialog(qm,"禁止0开头账号，密码", "提示", JOptionPane.INFORMATION_MESSAGE);
					
					String facepath=txtface.getText();//人脸路径
					if(facepath.length()>0) {//人脸+账号双注册
					String baiduzhucemess=Sample.Facezhuce(facepath,"QQ"+qqid.getText()+"PASS"+qqpass.getText());
					if(baiduzhucemess.indexOf("SUCCESS")>-1) {
					  String zhucesql=zhuceinfo();
					  if(zhucesql!=null) {
				         JOptionPane.showMessageDialog(qm,zhucesql, "提示", JOptionPane.INFORMATION_MESSAGE);
					  }
					}else {
						JOptionPane.showMessageDialog(qm,"人脸注册失败", "提示", JOptionPane.INFORMATION_MESSAGE);
					}}else {//单独账号注册
						  String zhucesql=zhuceinfo();
						  if(zhucesql!=null) {
					         JOptionPane.showMessageDialog(qm,zhucesql, "提示", JOptionPane.INFORMATION_MESSAGE);
						  }
					}
					
				}}).start();}
		});
		btnenter.setFont(new Font("黑体", Font.PLAIN, 12));
		btnenter.setBounds(321, 381, 103, 23);
		contentPane.add(btnenter);
		
		JLabel label_3 = new JLabel("\u5934 \u50CF\uFF1A");
		label_3.setFont(new Font("黑体", Font.PLAIN, 16));
		label_3.setBounds(29, 59, 64, 37);
		contentPane.add(label_3);
		
		touxiang = new JTextField();
		touxiang.setColumns(10);
		touxiang.setBounds(145, 63, 38, 21);
		contentPane.add(touxiang);
		
		JLabel label_4 = new JLabel("\uFF08\u5934\u50CF\u8F93\u5165 1-7 ,\u9ED8\u8BA4 1\uFF09");
		label_4.setFont(new Font("黑体", Font.PLAIN, 12));
		label_4.setBounds(222, 63, 147, 21);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("\uFF08\u4E2A\u6027\u7B7E\u540D\u9650\u523620\u4E2A\u5B57\u4EE5\u5185 \uFF09");
		label_5.setFont(new Font("黑体", Font.PLAIN, 12));
		label_5.setBounds(145, 205, 186, 21);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("\u4EBA\u8138\u767B\u5F55\uFF1A");
		label_6.setFont(new Font("黑体", Font.PLAIN, 16));
		label_6.setBounds(29, 334, 96, 21);
		contentPane.add(label_6);
		
		txtface = new JTextField();
		txtface.setColumns(10);
		txtface.setBounds(145, 335, 166, 21);
		contentPane.add(txtface);
		
		JButton btnface = new JButton("\u9009\u62E9");
		btnface.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 JFileChooser saveFile = new JFileChooser( "C:\\Users\\16631\\Pictures\\Camera Roll");
				 FileNameExtensionFilter filter = new FileNameExtensionFilter(
						 "*.png *.jpg 图片", "png", "jpg");
				 saveFile.addChoosableFileFilter(filter);
				 saveFile.setFileSelectionMode(JFileChooser.FILES_ONLY);
				 saveFile.showOpenDialog(f);
				 String facepath = saveFile.getSelectedFile().getAbsolutePath();
				 txtface.setText(facepath);
			}
		});
		btnface.setBounds(327, 334, 64, 23);
		contentPane.add(btnface);
		
		JLabel bg = new JLabel("");
		bg.setIcon(new ImageIcon(".\\icon\\zhucebg.jpg"));
		bg.setBounds(0, 0, 440, 425);
		contentPane.add(bg);
	}
	
	@SuppressWarnings("deprecation")
	public String zhuceinfo() {
		String tip="";
		String wname=name.getText();
		String qq=qqid.getText();
		String pass=qqpass.getText();
		String pass2=qqpass2.getText();
		String tx=touxiang.getText();
		String gxqm=qm.getText();
		if(wname.equals(""))
			tip=tip+"昵称不可以为空\n";
		if(qq.equals(""))
			tip=tip+"QQ不可以为空\n";
		if(pass.equals(""))
			tip=tip+"密码不可以为空\n";
		if(pass2.equals(""))
			tip=tip+"确认密码不可以为空\n";
		if(tx.equals(""))
			tx="1";
		for(int i=1;i<8;i++) {
			if(touxiang.getText().equals(""+i)) {
				break;
			}
			if(i==7) {
				tip=tip+"头像格式有误\n";
			}
			
			}
		
		if(gxqm.equals(""))
			gxqm="本宝宝还没想到好的签名";
		if(qm.getText().length()>20)
			tip=tip+"个性签名太长了\n";
		if(!pass.equals(pass2))
			tip=tip+"两次密码不一样  ";
		if(!tip.equals("")) {
			JOptionPane.showMessageDialog(qm,tip, "提示", JOptionPane.INFORMATION_MESSAGE);
			return null;
		}else {
			String zhucesql="insert into QQpersoninfo(qqid,qqpassword,qqqianming,qqicon,qqname)  " + 
			        " values('"+qq+"','"+pass+"','"+gxqm+"','"+tx+"','"+wname+"')";
			
			return fainfo(zhucesql);
			
		}
		
	}
	
	public String fainfo(String mess) {
		try {
			Socket cli=new Socket(ip,port);
			if(cli.isConnected()) {
				DataOutputStream out = new DataOutputStream(cli.getOutputStream());
				DataInputStream in = new DataInputStream(cli.getInputStream());
				out.writeUTF("%%%");
				out.writeUTF(mess);
				
				String remess=in.readUTF();
				if(remess.equals("成功")) {//返回信息  
					cli.close();
					return "注册成功";}
				else {
					cli.close();
					return "注册失败";
				}
				
				
			}else {
				cli.close();
				return "注册失败";
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "注册失败";
		
	}
}
