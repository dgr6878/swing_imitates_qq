package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;

public class QQ_addfriend extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField qq;
	private JTextField qun;


	public QQ_addfriend(int x,int y,final String myqq,final String ip,final int port) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("\u6DFB  \u52A0");
		setBounds(x, y, 320, 215);
		setResizable(false);//不可改变大小
		//setBounds(785, 130, 320, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u597D\u53CBQQ:");
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 16));
		lblNewLabel.setBounds(31, 34, 76, 27);
		contentPane.add(lblNewLabel);
		
		JLabel lblqq = new JLabel("\u7FA4  QQ:");
		lblqq.setFont(new Font("黑体", Font.PLAIN, 16));
		lblqq.setBounds(31, 71, 76, 27);
		contentPane.add(lblqq);
		
		qq = new JTextField();
		qq.setBounds(125, 38, 144, 21);
		contentPane.add(qq);
		qq.setColumns(10);
		
		qun = new JTextField();
		//qun.setEnabled(false);
		qun.setColumns(10);
		qun.setBounds(125, 75, 144, 21);
		contentPane.add(qun);
		
		JButton btnadd = new JButton("\u6DFB \u52A0");
		btnadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new Runnable() {

					@Override
					public void run() {

						if(qun.getText().length()>0&&qq.getText().length()>0)
							JOptionPane.showMessageDialog(qq,"一次只能加一个哦", "提示", JOptionPane.INFORMATION_MESSAGE);
						    
						if(qun.getText().length()==0&&qq.getText().length()==0)
							JOptionPane.showMessageDialog(qq,"不可以都为空哦", "提示", JOptionPane.INFORMATION_MESSAGE);
						String qqhao=qq.getText();String qunhao=qun.getText();
						if(qqhao.length()>0||qunhao.length()>0) {
							Socket cli;
							try {
								cli = new Socket(ip,port);
								DataOutputStream out = new DataOutputStream(cli.getOutputStream());
								DataInputStream in = new DataInputStream(cli.getInputStream());
								if(qqhao.length()>0) {
									out.writeUTF("%%%");
									String addsql="insert  " + 
											      "into QQfriend(qqid,friend) " + 
											       "values("+myqq+","+qqhao+")";
									String addsql2=" insert  " + 
										      " into QQfriend(qqid,friend) " + 
										       " values("+qqhao+","+myqq+")";
									String chasql="select qqid " + 
										          "from QQpersoninfo " + 
											      "where qqid="+qqhao;
									out.writeUTF(chasql);
//									Timer time=new Timer(true);
//									 TimerTask task = new TimerTask() {    
//								            public void run() {    
//								               setVisible(false); 
//								               Thread.currentThread().stop();
//								            }    
//								        };   
//								       // time.schedule(task, 20000);
									if(in.readUTF().equals("成功")) {
										cli.close();
										Socket cli2 = new Socket(ip,port);
										DataOutputStream out2 = new DataOutputStream(cli2.getOutputStream());
										DataInputStream in2 = new DataInputStream(cli2.getInputStream());
										out2.writeUTF("%%%");
										out2.writeUTF(addsql+addsql2);
										if(in2.readUTF().equals("成功"))
											JOptionPane.showMessageDialog(qq,"好友添加成功", "提示", JOptionPane.INFORMATION_MESSAGE);
											cli2.close();
									}else {
										JOptionPane.showMessageDialog(qq,"用户不存在", "提示", JOptionPane.INFORMATION_MESSAGE);
										
									}
									
								}
								if(qunhao.length()>0) {
									out.writeUTF("%%%");
									String addsql="insert  " + 
											      "into QQqunmember(qid,qmember) " + 
											       "values("+qunhao+","+myqq+") ";
									String chasql="select * " + 
										          "from QQquninfo " + 
											      "where qid="+qunhao;
									out.writeUTF(chasql);
									if(in.readUTF().equals("成功")) {
										cli.close();
										Socket cli2 = new Socket(ip,port);
										DataOutputStream out2 = new DataOutputStream(cli2.getOutputStream());
										DataInputStream in2 = new DataInputStream(cli2.getInputStream());
										out2.writeUTF("%%%");
										out2.writeUTF(addsql);
										if(in2.readUTF().equals("成功"))
											JOptionPane.showMessageDialog(qq,"群添加成功", "提示", JOptionPane.INFORMATION_MESSAGE);
											cli2.close();
									}else {
										JOptionPane.showMessageDialog(qq,"群不存在", "提示", JOptionPane.INFORMATION_MESSAGE);
									}
									
								}
							} catch (UnknownHostException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						}
						
						
					
						
					}}).start();
				
			}
		});
		btnadd.setFont(new Font("黑体", Font.PLAIN, 16));
		btnadd.setBounds(201, 137, 97, 23);
		contentPane.add(btnadd);
		
		JButton crequn = new JButton("\u521B\u5EFA\u7FA4\u804A");
		final QQ_CreateGroup qqcreatqun=new QQ_CreateGroup(this.getX()-80,this.getY(),myqq,ip,port);
		crequn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				qqcreatqun.setVisible(true);
				setVisible(false);
				
			}
		});
		crequn.setFont(new Font("黑体", Font.PLAIN, 16));
		crequn.setBounds(76, 138, 115, 23);
		contentPane.add(crequn);
		
		JLabel addbg = new JLabel("");
		addbg.setIcon(new ImageIcon(".\\icon\\addbg.jpg"));
		addbg.setBounds(0, 0, 322, 178);
		contentPane.add(addbg);
	}

	

}
