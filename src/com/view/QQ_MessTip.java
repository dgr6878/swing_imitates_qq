package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Font;

public class QQ_MessTip extends JFrame {

	/**
	 * 
	 */
	Dimension d = Toolkit.getDefaultToolkit().getScreenSize(); //获取全屏幕的宽高尺寸等数据
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel neirong;
	JLabel bg;

	/**
	 * Create the frame.
	 */
	public QQ_MessTip(String ss) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("QQ\u65B0\u6D88\u606F");
		setBounds(1300, 660, 243, 159);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		neirong = new JLabel(ss);
		neirong.setForeground(new Color(0, 0, 0));
		neirong.setFont(new Font("黑体", Font.PLAIN, 18));
		neirong.setBounds(0, 10, 219, 102);
		contentPane.add(neirong);
		
		bg = new JLabel("");
		bg.setBounds(0, 0, 229, 122);
		bg.setIcon(new ImageIcon(".\\icon\\messtip.jpg"));
		contentPane.add(bg);
        Timer time=new Timer(true);
        TimerTask task = new TimerTask() {    
            @SuppressWarnings("deprecation")
			public void run() {    
               setVisible(false); 
               Thread.currentThread().stop();
            }    
        };   
        time.schedule(task, 2000);

	}
	
	public QQ_MessTip(String ss,int judge) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("警告");
		setBounds((int) (d.getWidth()-243), (int) (d.getHeight()-159), 243, 159);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		neirong = new JLabel("     "+ss);
		neirong.setForeground(Color.RED);
		neirong.setFont(new Font("黑体", Font.PLAIN, 16));
		neirong.setBounds(0, 10, 219, 102);
		contentPane.add(neirong);
		
		bg = new JLabel("");
		bg.setBounds(0, 0, 229, 122);
		bg.setIcon(new ImageIcon(".\\icon\\noserver.png"));
		contentPane.add(bg);
	}
	public JLabel getNeirong() {
		return neirong;
	}

	public void setNeirong(String ss) {
		neirong.setText(ss);
	}
}
