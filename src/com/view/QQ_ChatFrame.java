package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class QQ_ChatFrame extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	FileInputStream fis;
	Socket client;
	QQ_ChatPane chattxt;
	JTextArea fainfo;
	JButton btnfasong;
	FileWriter fw;
	String qq;
	String myqq;
	
	
	

	public QQ_ChatFrame(final Socket client,final String qq,final String myqq,String name,String myicon,String dficon) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\"+dficon+".jpg"));
		this.client=client;
		this.qq=qq;
		this.myqq=myqq;
		jiantin jian=new jiantin(client);
		setFont(new Font("黑体", Font.BOLD, 26));
		setForeground(Color.RED);
		setTitle("                                                                        "+name);
		setBounds(470, 160, 608, 512);
		setResizable(false);//不可改变大小
	    addWindowListener(new WindowAdapter() {
	        public void windowClosing(WindowEvent e) {
	        	
	          setVisible(false);
	        }
	      });
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel bq = new JLabel("");
		bq.setIcon(new ImageIcon(".\\icon\\bq.gif"));
		bq.setBackground(Color.LIGHT_GRAY);
		bq.setBounds(20, 278, 34, 30);
		bq.setOpaque(false);
		contentPane.add(bq);
	
		
		
		chattxt = new QQ_ChatPane(contentPane,myqq,qq,myicon,dficon);
		
		
		
	
		
		
	    fainfo = new JTextArea();
	    fainfo.addKeyListener(new KeyAdapter() {
	    	@Override
	    	public void keyPressed(KeyEvent e) {
	    		if(e.getKeyCode()==KeyEvent.VK_ENTER) {

	    			  String messaddress="#"+myqq+"#"+qq+"#";
	    			  String tomess=fainfo.getText();
	    			  DataOutputStream out;
	    			  try {
	    				out = new DataOutputStream(client.getOutputStream());
	    				out.writeUTF(messaddress+tomess);
	    				fw=new FileWriter(".//chatrecord//#"+myqq+"//#"+qq+"#"+myqq+".txt",true);
	    			    fw.append(messaddress+tomess+"\r\n");
	    			    fw.close();
	    				fainfo.setText("");
	    			  } catch (IOException e1) {
	    				e1.printStackTrace();}
	    		
	    		}
	    	}
	    	@Override
	    	public void keyReleased(KeyEvent e) {
	    		if(e.getKeyCode()==KeyEvent.VK_ENTER) {
	    			fainfo.setText("");
	    		}
	    	}
	    });
	    fainfo.setLineWrap(true);
	    contentPane.setFocusable(true);//给p提供焦点．
	
	    fainfo.setFont(new Font("Adobe 黑体 Std R", Font.PLAIN, 16));
	    fainfo.setBounds(10, 311, 442, 116);
	    contentPane.add(fainfo);
	  
	 
		
		btnfasong = new JButton("\u53D1  \u9001");
		btnfasong.setFont(new Font("宋体", Font.PLAIN, 16));
		btnfasong.setBounds(353, 437, 97, 30);
		contentPane.add(btnfasong);
		btnfasong.addActionListener(jian);
		
		JButton button = new JButton("\u5173  \u95ED");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		button.setFont(new Font("宋体", Font.PLAIN, 16));
		button.setBounds(235, 437, 97, 30);
		contentPane.add(button);
		
		JLabel bg = new JLabel("");
		bg.setIcon(new ImageIcon(".\\icon\\02.jpg"));
		bg.setBounds(0, 0, 594, 476);
		contentPane.add(bg);
		
		 new Thread(new Runnable() {

			@SuppressWarnings("static-access")
			@Override
			public void run() {
				try {
					while(true) {
						Thread.currentThread().sleep(200);
					fis = new FileInputStream(".//chatrecord//#"+myqq+"//#"+qq+"#"+myqq+".txt");
					byte[] content = new byte[fis.available()];
					fis.read(content);	
					if(!chattxt.gettext().equals(new String(content))) {
					      chattxt.settext(".//chatrecord//#"+myqq+"//#"+qq+"#"+myqq+".txt");
						//chattxt.setchat("#12345#12345#666");
					}
					
					}
					
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch( InterruptedException e) {}
	     
		        }
			
				
			}).start();
		

	}
	
	
	public String bigjiansmall(String big,String small) {
		return big.substring(big.indexOf(small)+small.length());
		
	}
	
class jiantin implements ActionListener{

	Socket client;
	
	
	Thread shipin;
	jiantin(Socket client){
		this.client=client;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==btnfasong) {
		  String messaddress="#"+myqq+"#"+qq+"#";
		  String tomess=fainfo.getText()+"\r\n";
		  DataOutputStream out;
		  try {
			out = new DataOutputStream(client.getOutputStream());
			out.writeUTF(messaddress+tomess);
			fw=new FileWriter(".//chatrecord//#"+myqq+"//#"+qq+"#"+myqq+".txt",true);
		    fw.append(messaddress+tomess);
		    fw.close();
			fainfo.setText("");
		  } catch (IOException e1) {
			e1.printStackTrace();}
	
		
			
			
			
		}
		
	}
	
	
	
  }
}
