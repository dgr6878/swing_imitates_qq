package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.MouseAdapter;

public class QQ_del extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	Mousejian  jian=new Mousejian();
	JLabel del;
	JLabel delqun;
	JLabel look;
	JLabel exit;
	String ip;
	int port;
	Socket cli;
    String myqq;
    String[] oneinfo;

	public QQ_del(int x,int y,String[] oneinfo,String myqq,String ip,int port) {
		this.ip=ip;
		this.port=port;
		this.myqq=myqq;
		this.oneinfo=oneinfo;
		setUndecorated(true);//无边框
		setBounds(x, y, 99, 149);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		look = new JLabel("   \u67E5\u770B\u8D44\u6599");
		look.setOpaque(true);
		look.setFont(new Font("黑体", Font.PLAIN, 14));
		look.setBackground(Color.white);
		look.setBounds(0, 1, 100, 36);
		look.addMouseListener(jian);
		contentPane.add(look);
		
		del = new JLabel("   \u5220\u9664\u597D\u53CB");
		del.setOpaque(true);
		del.setFont(new Font("黑体", Font.PLAIN, 14));
		del.setBackground(Color.white);
		del.setBounds(0, 38, 100, 36);
		del.addMouseListener(jian);
		contentPane.add(del);
		
		delqun = new JLabel("   删除群");
		delqun.setOpaque(true);
		delqun.setFont(new Font("黑体", Font.PLAIN, 14));
		delqun.setBackground(Color.white);
		delqun.setBounds(0, 75, 100, 36);
		delqun.addMouseListener(jian);
		contentPane.add(delqun);
		
		
		exit = new JLabel("   \u9000\u51FA");
		exit.setOpaque(true);
		exit.setFont(new Font("黑体", Font.PLAIN, 14));
		exit.setBackground(Color.white);
		exit.setBounds(0, 112, 100, 36);
		exit.addMouseListener(jian);
		contentPane.add(exit);
		
		Timer time=new Timer(true);
	    TimerTask task = new TimerTask() {    
	            public void run() {    
	               setVisible(false); 
	            }    
	        };   
	    time.schedule(task, 7000);
		
		
	}

class Mousejian implements MouseListener{
		
		@Override
		public void mouseClicked(MouseEvent e) {
			int clickcount=e.getClickCount();
			if(clickcount==1) {//单击
				QQ_xiugai ziliao=new QQ_xiugai(500,200,oneinfo[0],oneinfo[2],oneinfo[3],oneinfo[1]);
			    if(e.getSource()==look) {
			    	setVisible(false);
			    	ziliao.setVisible(true);
			    }else if(e.getSource()==del) {
			    	delfri(oneinfo[0]);
			    	
			    }else if(e.getSource()==exit) {
			    	setVisible(false);
			    }else if(e.getSource()==delqun) {
			    	delqun(oneinfo[0]);
			    }
				
				
			}
			
			
		}
		
		public void delfri(String qq) {
			
			try {
				cli = new Socket(ip,port);
				DataOutputStream out = new DataOutputStream(cli.getOutputStream());
				DataInputStream in = new DataInputStream(cli.getInputStream());
				out.writeUTF("%%%");
				String sql1="  delete " + 
						      "from QQfriend " + 
					          "where qqid="+myqq+" and friend="+qq+"    ";
						
				String sql2="  delete  " + 
						      "  from QQfriend " + 
						      "  where qqid="+qq+" and friend="+myqq;
				out.writeUTF(sql1+sql2);
				String flag=in.readUTF();
				if(flag.equals("成功")) {
						JOptionPane.showMessageDialog(look,"已经删除好友", "提示", JOptionPane.INFORMATION_MESSAGE);
						
						
				}
				if(flag.equals("失败")){
					   JOptionPane.showMessageDialog(look,"删除失败", "提示", JOptionPane.INFORMATION_MESSAGE);
				}
				setVisible(false);
					
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
			
		}
		
	public void delqun(String qun) {
			
			try {
				cli = new Socket(ip,port);
				DataOutputStream out = new DataOutputStream(cli.getOutputStream());
				DataInputStream in = new DataInputStream(cli.getInputStream());
				out.writeUTF("%%%");
				String sql1="  delete  " + 
						      "from QQqunmember " + 
					          "where qid="+qun+" and qmember="+myqq+"    ";
						
				out.writeUTF(sql1);
				String flag=in.readUTF();
				if(flag.equals("成功")) {
						JOptionPane.showMessageDialog(look,"已退出群", "提示", JOptionPane.INFORMATION_MESSAGE);
						
						
				}
				if(flag.equals("失败")){
					   JOptionPane.showMessageDialog(look,"退出失败", "提示", JOptionPane.INFORMATION_MESSAGE);
				}
				setVisible(false);
					
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			
			    if(e.getSource()==del)
				       del.setBackground(Color.LIGHT_GRAY);
				if(e.getSource()==look)
					   look.setBackground(Color.LIGHT_GRAY);
				if(e.getSource()==exit)
					   exit.setBackground(Color.LIGHT_GRAY);
				if(e.getSource()==delqun)
					   delqun.setBackground(Color.LIGHT_GRAY);
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			    if(e.getSource()==del)
				       del.setBackground(Color.WHITE);
				if(e.getSource()==look)
					   look.setBackground(Color.WHITE);
				if(e.getSource()==exit)
					   exit.setBackground(Color.WHITE);
				if(e.getSource()==delqun)
					  delqun.setBackground(Color.WHITE);
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
}
	
	
	
}
