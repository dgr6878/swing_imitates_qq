package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.Recinfo.MySystemTray;
import com.Recinfo.QQ_heartAndLogininfo;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.net.Socket;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;

public class QQ_person extends JFrame {
    
	//变量声明
	private static final long serialVersionUID = 1L;
	QQ_heartAndLogininfo hea;
	private JFrame jf=this;
	JButton btntouxiang;
	private JPanel contentPane;
	JLabel name;
	QQ_MoveLabel qianming;
	String qqid;
	JLabel upbackground;
    public QQ_person frame;
	Mouse jian=new Mouse();
	QQ_people mess;
	QQ_people peo;
	QQ_people qun;
	Socket client;
    String logininfo="";
    String ip;
    int port;
    int closenum=1;
	//构造函数
    
    
	public QQ_person(Socket to,String logininfo,String qqid,String ip,int port,String mima) {
		
		
		
		this.qqid=qqid;
		client=to;
		this.logininfo=logininfo;
		this.ip=ip;
		this.port=port;
		String[] allinfo=getfriqunmess(logininfo);
		String[] friinfo=getallpersoninfo(allinfo[0]);
		String[] quninfo=getallpersoninfo(allinfo[1]);
		String[] messinfo=getallpersoninfo(allinfo[2]);
//		System.out.println("朋友信息"+allinfo[0]);
//		System.out.println("群信息"+allinfo[1]);
//		System.out.println("mess信息"+allinfo[2]);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setBounds(1100, 130, 264, 564);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.addMouseListener(jian);
		
		mess=new QQ_people(0,140,265,425,contentPane,client,messinfo,qqid);
		peo=new QQ_people(0,140,265,425,contentPane,client,friinfo,qqid);
		qun=new QQ_people(0,140,265,425,contentPane,client,quninfo,qqid);
		 mess.js.setVisible(false);
		 qun.js.setVisible(false);
		
		final JButton btnclose = new JButton("");
		btnclose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent arg0) {
				btnclose.setContentAreaFilled(false);
			}
		});
		btnclose.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				btnclose.setContentAreaFilled(true);
				btnclose.setBackground(Color.red);
				
			}
			
			
		});
		final QQ_exitTip exitframe = new QQ_exitTip(jf);
		exitframe.setVisible(false);
		btnclose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(closenum==1) {
					exitframe.setBounds(getX(), getY()+100, 231, 167);
					exitframe.setVisible(true);
					closenum++;
				}else {
					if(exitframe.getRem().isSelected()) {
						if(exitframe.getMin().isSelected()) {
							setVisible(false);
						}
						if(exitframe.getExit().isSelected()) {System.exit(0);}
					}else {
						exitframe.setBounds(getX(), getY()+100, 231, 167);
						exitframe.setVisible(true);
						
					}
				}
				
				
				
			}
		});
		btnclose.setIcon(new ImageIcon(".\\icon\\close2.gif"));
		btnclose.setBounds(243, 0, 20, 20);
		contentPane.add(btnclose);
		
		JButton btnzaixian = new JButton("");
		btnzaixian.setIcon(new ImageIcon(".\\icon\\zaixian.gif"));
		btnzaixian.setBounds(49, 65, 21, 21);
		btnzaixian.setContentAreaFilled(false);
		btnzaixian.setBorderPainted(false);//边框线
		contentPane.add(btnzaixian);
		
		JButton btnminframe = new JButton("");
		btnminframe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setExtendedState(JFrame.ICONIFIED);//最小化窗体
			}
		});
		btnminframe.setIcon(new ImageIcon(".\\icon\\minframe.gif"));
		btnminframe.setBounds(216, 0, 20, 20);
		btnminframe.setContentAreaFilled(false);
		contentPane.add(btnminframe);
		
		JButton btnmess = new JButton("\u6D88 \u606F");
		btnmess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 
				 peo.js.setVisible(false);
				 qun.js.setVisible(false);
				 mess.js.setVisible(true);
			}
		});
		btnmess.setBounds(0, 118, 87, 23);
		btnmess.setContentAreaFilled(false);
		//btnmess.setBorderPainted(false);//边框线
		contentPane.add(btnmess);
		
		JButton btnperson = new JButton("\u8054\u7CFB\u4EBA");
		btnperson.setContentAreaFilled(false);
		btnperson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 mess.js.setVisible(false);
				 qun.js.setVisible(false);
				 peo.js.setVisible(true);
			}
		});
		btnperson.setBounds(87, 118, 87, 23);
		contentPane.add(btnperson);
		
		JButton btnqun = new JButton("\u7FA4  \u7EC4");
		btnqun.setContentAreaFilled(false);
		
		btnqun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 peo.js.setVisible(false);
				 mess.js.setVisible(false);
				 qun.js.setVisible(true);
			}
		});
		btnqun.setBounds(172, 118, 87, 23);
		contentPane.add(btnqun);
		String[] me=new String[4];
		me=getoneinfo(allinfo[0]);
		String[] me2=me;
		btntouxiang = new JButton();
		final QQ_xiugai frame = new QQ_xiugai(jf.getX()-395,jf.getY(),me2[0],me2[2],me2[3],me2[1].trim(),me2[0],ip,port);
		frame.setVisible(false);
		btntouxiang.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setBounds(jf.getX()-395,jf.getY(),frame.getWidth(),frame.getHeight());
				frame.setVisible(true);
				String[] updateinfo=frame.reinfo();
				name.setText(updateinfo[1]);//没反应
				btntouxiang.setIcon(new ImageIcon(".\\icon\\"+updateinfo[0]+".jpg"));
				qianming.setText(updateinfo[2]);
				btntouxiang.validate();
				
			}
		});
		
		
		
		hea=new QQ_heartAndLogininfo(ip,port,me2[0],mima);//qqhao  +   mima
		hea.start();//不断获取所有信息，间隔1秒
		
		
		
		btntouxiang.setIcon(new ImageIcon(".\\icon\\"+me[2]+".jpg"));
		btntouxiang.setBounds(12, 30, 47, 47);
		contentPane.add(btntouxiang);
		
		JButton btntianqi = new JButton("");
		btntianqi.setIcon(new ImageIcon(".\\icon\\天气.gif"));
		btntianqi.setBounds(216, 30, 47, 47);
		btntianqi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			  
				
			}
		});
		contentPane.add(btntianqi);
		
		JButton btnadd = new JButton();
		btnadd.setIcon(new ImageIcon(".\\icon\\add.jpg"));
		btnadd.setBounds(230, 80, 30, 30);
		final QQ_addfriend qqadd = new QQ_addfriend(jf.getX()-315,jf.getY(),me2[0],ip,port);
		qqadd.setVisible(false);
		btnadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				qqadd.setBounds(jf.getX()-315,jf.getY(), qqadd.getWidth(), qqadd.getHeight());
				qqadd.setVisible(true);
				
			}
		});
		contentPane.add(btnadd);
		
		name = new JLabel(me[3]);
		name.setFont(new Font("宋体", Font.BOLD, 14));
		name.setForeground(new Color(255, 255, 255));
		name.setBounds(70, 30, 80, 15);
		contentPane.add(name);
		
		qianming = new QQ_MoveLabel(me[1]);
		qianming.setOpaque(true);
		//qianming.setForeground(new Color(255, 255, 255));
		qianming.setBackground(Color.yellow);
		qianming.setBounds(70, 55, 104, 15);
		contentPane.add(qianming);
		
		JLabel level = new JLabel("");
		level.setFont(new Font("宋体", Font.PLAIN, 8));
		level.setIcon(new ImageIcon(".\\icon\\level.gif"));//等级图片
		level.setBounds(133, 30, 30, 20);
		contentPane.add(level);
		
		upbackground = new JLabel("");
		upbackground.setIcon(new ImageIcon(".\\icon\\bg.jpg"));
		upbackground.setBounds(0, 0, 264, 564);
		contentPane.add(upbackground);
		
		//设置托盘区实例
		MySystemTray mytray=new MySystemTray(me[3],qqid,me[2],jf,to,logininfo);
		mytray.yinca();
		
		
  		
	   new Thread(new Runnable() {

		@SuppressWarnings("static-access")
		@Override
		public void run() {
			while(true) {
				try {
					Thread.currentThread().sleep(1000);
					refresh();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}}).start();
		
		
	}//构造结束
	
public void refresh() {
			String freshlogininfo=hea.getlogininfo();
			if(!freshlogininfo.equals(logininfo)) {
				System.out.println("信息已更新");
				logininfo=freshlogininfo;
				String[] allinfo=getfriqunmess(freshlogininfo);
				String[] friinfo=getallpersoninfo(allinfo[0]);
				String[] quninfo=getallpersoninfo(allinfo[1]);
				//String[] messinfo=getallpersoninfo(allinfo[2]);
				String[] me=getoneinfo(allinfo[0]);
				   name.setText(me[3]);
				   qianming.setText(me[1]);
				   btntouxiang.setIcon(new ImageIcon(".\\icon\\"+me[2]+".jpg"));
				   //peo.removejs();
				   //qun.removejs();
				    peo.premoveall();
				    qun.premoveall();
				    peo.rehuaone(friinfo);
				    qun.rehuaone(quninfo);
				 
				//mess=new QQ_people(0,140,265,425,contentPane,client,messinfo,qqid);
				//peo=new QQ_people(0,140,265,425,contentPane,client,friinfo,qqid);
				//qun=new QQ_people(0,140,265,425,contentPane,client,quninfo,qqid);
				
				
				 mess.js.setVisible(false);
				 qun.js.setVisible(false);
				 peo.js.setVisible(true);
				 contentPane.revalidate();
				 
				 
			}
		
    	
   
	
			

	}

	
	
public String[] getfriqunmess(String logininfo) {
	String[] info=new String[3];
	int a1=0;
	int a2=0;
	int a3=0;
	int a4=0;
	a1=logininfo.indexOf("@");
	a2=logininfo.indexOf("@",a1+1);
	a3=logininfo.indexOf("@",a2+1);
	a4=logininfo.indexOf("@",a3+1);
	info[0]=logininfo.substring(a1+1, a2);
	info[1]=logininfo.substring(a2+1,a3);
	info[2]=logininfo.substring(a3+1,a4);
	
	
	return info;	
}
	
public int getrennum(String logininfo) {
	int num=0;
	int a=0;
	while(true) {
		a=logininfo.indexOf("$",a);
		
		if(a>-1) {
			num++;
		    a=a+1;
		}
		
		else {break;}
	}
	num=num/2;
	
	return num;
}
public String[] getoneinfo(String ss) {
	String[] oneinfo=new String[4];
	int start2=0;
	int end2=0;
	for(int i=0;i<4;i++) {
		
		if(i<1) {
			start2=ss.indexOf("#");
			end2=ss.indexOf("#",start2+1);
			String name=ss.substring(start2+1, end2);
			oneinfo[i]=name;
		}else {
			start2=ss.indexOf("#",end2);
			end2=ss.indexOf("#",start2+1);
			String name=ss.substring(start2+1, end2);
			oneinfo[i]=name;
			
		}	
	}
	return oneinfo;
}

public String[] getallpersoninfo(String logininfo) {
	String[] allperson=new String[getrennum(logininfo)];
	int start2=0;
	int end2=0;
	for(int i=0;i<getrennum(logininfo);i++) {
		
		if(i<1) {
			start2=logininfo.indexOf("$");
			end2=logininfo.indexOf("$",start2+1);
			allperson[i]=logininfo.substring(start2+1, end2);
		}else {
			start2=logininfo.indexOf("$",end2+1);
			end2=logininfo.indexOf("$",start2+1);
			allperson[i]=logininfo.substring(start2+1, end2);	
		}
		
		
	}
	return allperson;
	
	
}

	
class Mouse implements MouseListener{
		int x1,x2,y1,y2;

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			
			x1=e.getX();
			y1=e.getY();
			
			
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			x2=e.getX();
			y2=e.getY();
			int x3=frame.getX();
			int y3=frame.getY();
	        frame.setBounds(x3+(x2-x1), y3+(y2-y1), frame.upbackground.getWidth(), frame.upbackground.getHeight());

			
		}
  }

class mousepanemove implements MouseMotionListener{

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		int x1=e.getX();
		int y1=e.getY();
		frame.setBounds(x1, y1, frame.upbackground.getWidth(), frame.upbackground.getHeight());
		
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
}



