package com.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class QQ_ChatPane {
	
	JPanel jp;
	JPanel p;//总共的
	JScrollPane js;
	int paneheight=258;
	String myqq;
	String myicon;
	String allinfo="";//所有的聊天信息
	String dficon;
	String duifangqq;
	int linenum=12;//一行的字数
	static Font font=new Font("Adobe 黑体 Std R", Font.PLAIN, 16);
	int totleheight=1;
	JScrollBar bar;
	
	QQ_ChatPane(JPanel jp,String myqq,String duifangqq,String myicon,String dficon){
	     this.myqq=myqq;
	     this.jp=jp;
	     this.duifangqq =duifangqq;
	     this.myicon=myicon;
	     this.dficon=dficon;
	     p=new JPanel();
	     p.setBackground(Color.WHITE);
	     p.setLayout(null);
		 js=new JScrollPane(p);
		 js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		 js.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		 js.getVerticalScrollBar().setUnitIncrement(10);
		 js.setBounds(10, 10, 442, 258);
		 bar=js.getVerticalScrollBar();
		 bar.setValue(bar.getMaximum());
		 p.setPreferredSize(new Dimension(442,paneheight+100));
		 p.validate();
		 jp.add(js);
		 //settext("C:\\Users\\16631\\Desktop\\常用文件夹\\QQ\\chatrecord\\#12345\\#54321#12345.txt");	
	}
	
	
public void settext(String path) {
	FileInputStream fis = null;
	try {
	FileReader fr = new FileReader(path);
	BufferedReader bf = new BufferedReader(fr);
	String str="";
	
	// 按行读取字符串
	while ((str = bf.readLine()) != null) {
		setchat(str);
	}

		fis = new FileInputStream(path);
		byte[] content = new byte[fis.available()];
		fis.read(content);
		allinfo=new String(content);
		
		bf.close();
	} catch (IOException e) {
		try {
			fis.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		e.printStackTrace();
	}	
	
	
	
	
	
	
}

public String gettext(String path) {
	FileInputStream fis;
	try {
		fis = new FileInputStream(path);
		byte[] content = new byte[fis.available()];
	    fis.read(content);
		allinfo=new String(content);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return allinfo;
}
public String gettext() {
	return allinfo;
}
	
public void setchat(String ss) {

	String[] info=new String[3];
	info[0]="";
	info[1]="";
	info[2]="";
	int a1=0;
	int a2=0;
	int a3=0;
	a1=ss.indexOf("#");
	a2=ss.indexOf("#",a1+1);
	a3=ss.indexOf("#",a2+1);
	if(a2>a1)
	info[0]=ss.substring(a1+1, a2);//from
	if(a3>a2)
	info[1]=ss.substring(a2+1,a3);//to
	info[2]=ss.substring(a3+1);//mess
//	System.out.println("from  "+info[0]);
//	System.out.println("to  "+info[1]);
//	System.out.println("mess  "+info[2]);
//	System.out.println("myqq  "+myqq);
//	System.out.println("对方qq  "+duifangqq);
	if(info[0].equals(myqq)) {
		mysay(info[2]);
	}
	if(info[0].equals(duifangqq)) {
		duifangsay(info[2]);
	}
	
}
	
public String zhuan(String mess) {
	
	String st="<html>";
	String end="</html>";
	String huan="<br>";
	String s="";
	int a=0;
	if(mess.length()<=linenum) {
		return mess;
	}else {
		for(int i=0;i<getline(mess);i++) {
			if((i+1)==getline(mess)) {
				
				s=s+mess.substring(a)+huan;
			}else {
			s=s+mess.substring(a, a+12)+huan;
			a=a+12;
			}
		}
		
		
	}

	
	return st+s+huan+end;
}

public void duifangsay(String mess) {
	String iconpath=".\\icon\\"+dficon+".jpg";
    int  width=240;
	
    JPanel dypane=new JPanel();
    dypane.setVisible(true);
    dypane.setLayout(null);
    dypane.setOpaque(false);
    if(getline(mess)>1) {
        dypane.setBounds(10, totleheight, js.getWidth()-30, getline(mess)*60);
    }
    else {
    dypane.setBounds(10, totleheight, js.getWidth()-30, 60);
    }
    p.add(dypane);
    totleheight= totleheight+dypane.getHeight()+10;
    
    JLabel icon=new JLabel();
    icon.setBounds(5, 5, 50, 50);
    icon.setIcon(new ImageIcon(iconpath));
    dypane.add(icon);
    
    
    
    JLabel message=new JLabel(zhuan(mess),JLabel.CENTER);
    message.setOpaque(true);
    message.setBackground(Color.yellow);
    message.setFont(font);
    if(getline(mess)<=1) {
    	
    	message.setBounds(60,5,width,30 );
    }else {
    message.setBounds(60,5,width, getline(mess)*30+70);
    }
    p.setPreferredSize(new Dimension(442,totleheight));
    dypane.add(message);
    dypane.validate();
    p.validate();
    jp.validate();
    bar.setValue(bar.getMaximum());
   } 

public void mysay(String mess) {
	String iconpath=".\\icon\\"+myicon+".jpg";
    int  width=240;
	
    JPanel dypane=new JPanel();
    dypane.setVisible(true);
    dypane.setLayout(null);
    dypane.setOpaque(false);
    if(getline(mess)>1) {
        dypane.setBounds(10, totleheight, js.getWidth()-30, getline(mess)*45);
    }
    else {
    dypane.setBounds(10, totleheight, js.getWidth()-30, 60);
    }
    p.add(dypane);
    totleheight= totleheight+dypane.getHeight()+10;
    
    JLabel icon=new JLabel();
    icon.setBounds(350, 5, 50, 50);
    icon.setIcon(new ImageIcon(iconpath));
    dypane.add(icon);
    
    
    
    
    JLabel message=new JLabel(zhuan(mess),JLabel.CENTER);
    
    message.setOpaque(true);
    message.setBackground(Color.yellow);
    message.setFont(font);
    if(getline(mess)<=1) {
    	
    	message.setBounds(100,5,width,30 );
    }else {
        message.setBounds(100,5,width, getline(mess)*45);
    }
    p.setPreferredSize(new Dimension(442,totleheight));
	 
 	 p.validate();
    dypane.add(message);
    dypane.validate();
    p.validate();
    jp.validate();	
    bar.setValue(bar.getMaximum());
   } 



public int getline(String mess) {
	
	int line=mess.length()/linenum;
	
	if(mess.length()%linenum>0)
		line++;
	
	return line;	
}
public void setMyicon(String myicon) {
	this.myicon = myicon;
}

public void setDficon(String dficon) {
	this.dficon = dficon;
}
	

}


