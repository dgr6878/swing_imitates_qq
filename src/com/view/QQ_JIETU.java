package com.view;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
 
public class QQ_JIETU {
 
    private String path; // 默认前缀（选择存储路径例如： D：\\）
    private String imageFormat="png"; // 图像文件的格式
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize(); //获取全屏幕的宽高尺寸等数据
    String id;
 
    public QQ_JIETU(String id,String path,int x,int y,int w,int h) {
        this.path = path;
        this.id=id;
        if(id.equals(""))
        	this.id="default";
        snapShot(x,y,w,h);
        
    }
 
    public void snapShot(int x,int y,int w,int h) {
        try {
            // *** 核心代码 *** 拷贝屏幕到一个BufferedImage对象screenshot
            BufferedImage screenshot = (new Robot()).createScreenCapture(new Rectangle(x, y, w, h));
            // 根据文件前缀变量和文件格式变量，自动生成文件名
            String name = id + "." + imageFormat;
            File f = new File(path+"\\"+name);
            System.out.print("Save File " + name);
            // 将screenshot对象写入图像文件
            ImageIO.write(screenshot, imageFormat, f);
            System.out.print("..Finished!\n");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
// 运行之后，即可将全屏幕截图保存到指定的目录下面<br>　　　　 // 配合前端页面上面的选择尺寸等逻辑，传到后台，即可实现自由选择截图区域和大小的截图<br>
 
}

