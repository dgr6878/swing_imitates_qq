package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Toolkit;
import javax.swing.JCheckBox;

public class QQ_exitTip extends JFrame {

	private JPanel contentPane;
	private JRadioButton min;
	private JRadioButton exit;
    boolean exitflag=false;
    private JCheckBox rem;

	public JRadioButton getMin() {
		return min;
	}



	public void setMin(JRadioButton min) {
		this.min = min;
	}



	public JRadioButton getExit() {
		return exit;
	}



	public void setExit(JRadioButton exit) {
		this.exit = exit;
	}



	public QQ_exitTip(final JFrame mainjf) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq2.gif"));
		setTitle(" \u63D0\u793A");
		setBounds(mainjf.getX(), mainjf.getY()+100, 231, 167);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	    min = new JRadioButton("\u6700\u5C0F\u5316\u5230\u6258\u76D8\u533A",true);
		min.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
			}
		});
		min.setFont(new Font("黑体", Font.PLAIN, 14));
		min.setBounds(34, 20, 147, 23);
		contentPane.add(min);
		
		JButton enter = new JButton("\u786E\u8BA4");
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(min.isSelected()) {
					mainjf.setVisible(false);
					setVisible(false);
				}
				if(exit.isSelected()) {
					System.exit(0);
				}
			}
		});
		enter.setFont(new Font("宋体", Font.PLAIN, 11));
		enter.setBounds(65, 101, 66, 23);
		contentPane.add(enter);
		
		exit = new JRadioButton("\u9000\u51FA\u7A0B\u5E8F");
		exit.setFont(new Font("黑体", Font.PLAIN, 14));
		exit.setBounds(34, 57, 127, 23);
		contentPane.add(exit);
		
		
		rem = new JCheckBox("\u8BB0\u4F4F",true);
		rem.setBounds(6, 100, 53, 23);
		contentPane.add(rem);
		
		
		ButtonGroup group=new ButtonGroup();
        //把单选按钮添加到按钮组中，这样只能选组中的一个按钮，真正实现单选
        group.add(exit);
        group.add(min);
		JButton quxiao = new JButton("\u53D6\u6D88");
		quxiao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				rem.setSelected(false);
			}
		});
		quxiao.setFont(new Font("宋体", Font.PLAIN, 11));
		quxiao.setBounds(141, 101, 66, 23);
		contentPane.add(quxiao);
		
		
	}



	public JCheckBox getRem() {
		return rem;
	}



	public void setRem(JCheckBox rem) {
		this.rem = rem;
	}
}
