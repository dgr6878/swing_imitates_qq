package com.view;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class QQ_CreateGroup extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField qname;
	private JTextField icontxt;
	private JTextArea gg;
	private DataOutputStream out;
	private DataInputStream in;
	private JTextField qid;
	int port;
	private Socket cli;


	public QQ_CreateGroup(int x,int y,final String myqq,final String ip,final int port) {
		this.port=port;
		setFont(new Font("黑体", Font.PLAIN, 16));
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\icon\\qq.gif"));
		setTitle("\u521B\u5EFAQQ\u7FA4");
		setBounds(x, y, 399, 261);
		setResizable(false);//不可改变大小
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JLabel icon = new JLabel("");
		icon.setBounds(27, 22, 50, 50);
		contentPane.add(icon);
		
		qname = new JTextField();
		qname.setBounds(178, 22, 179, 25);
		qname.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (qname.getText().length()<10) {
					
				} else {
				e.consume();
				}
				}
		});
		contentPane.add(qname);
		qname.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("\u7FA4\u6635\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("黑体", Font.PLAIN, 16));
		lblNewLabel.setBounds(105, 22, 80, 31);
		contentPane.add(lblNewLabel);
		
		JLabel label = new JLabel("\u7FA4\u516C\u544A\uFF1A");
		label.setFont(new Font("黑体", Font.PLAIN, 16));
		label.setBounds(105, 63, 80, 31);
		contentPane.add(label);
		
		qid = new JTextField();
		
		gg = new JTextArea();
		gg.setText("本宝宝不想写群公告！！！");
		gg.setLineWrap(true);
		gg.setBounds(178, 57, 179, 104);
		contentPane.add(gg);
		
		icontxt = new JTextField();
		icontxt.setText("9");
		icontxt.setBounds(39, 80, 43, 21);
		icontxt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar=e.getKeyChar();
				if (keyChar>=KeyEvent.VK_0 && keyChar<=KeyEvent.VK_9&&icontxt.getText().length()<2) {
					
				} else {
				e.consume();
				}
				}
		});
		contentPane.add(icontxt);
		icontxt.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("\u5934\u50CF\uFF1A");
		lblNewLabel_1.setBounds(10, 82, 43, 15);
		contentPane.add(lblNewLabel_1);
		
		JButton btnenter = new JButton("\u521B  \u5EFA");
		btnenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String tip="";
				if(qname.getText().equals(""))
					tip=tip+"群昵称不可以为空\n";
				if(icontxt.getText().equals(""))
					tip=tip+"头像不可以为空\n";
				if(qid.getText().equals(""))
					tip=tip+"群号码不可以为空\n";
				
				if(gg.getText().length()>20)
					tip=tip+"公告太长了\n";
			
				if(!tip.equals("")) {
					JOptionPane.showMessageDialog(gg,tip, "提示", JOptionPane.INFORMATION_MESSAGE);
				}else {
				
				
				
				
				
				try {
					cli=new Socket(ip,port);
					out = new DataOutputStream(cli.getOutputStream());
					in = new DataInputStream(cli.getInputStream());
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					String sql1="insert  "+
							"into QQquninfo(qid,qgonggao,qname,qowner,qtx)  "+
							"values ("+qid.getText()+",'"+gg.getText()+"','"+qname.getText()+"',"+myqq+","+icontxt.getText()+")";

							
					String sql2=" insert   " + 
							"into QQqunmember(qid,qmember)\r\n" + 
							"values("+qid.getText()+","+myqq+")";
				
				
					
					out.writeUTF("%%%");
					out.writeUTF(sql1+sql2);
					if(in.readUTF().equals("成功")) {
						 JOptionPane.showMessageDialog(gg,"创建成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
						 
					}else {
						JOptionPane.showMessageDialog(gg,"创建失败", "提示", JOptionPane.INFORMATION_MESSAGE);
					}
					//cli.close();
					setVisible(false);
					
				} catch (IOException e) {
					JOptionPane.showMessageDialog(gg,"服务器连接异常", "提示", JOptionPane.INFORMATION_MESSAGE);
					e.printStackTrace();
					try {
						cli.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}}
		});
		btnenter.setBounds(278, 184, 97, 23);
		contentPane.add(btnenter);
		
		JLabel lblqq = new JLabel("\u7FA4QQ\uFF1A");
		lblqq.setBounds(10, 127, 58, 15);
		contentPane.add(lblqq);
		
	
		qid.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar=e.getKeyChar();
				if (keyChar>=KeyEvent.VK_0 && keyChar<=KeyEvent.VK_9&&qid.getText().length()<10) {
					
				} else {
				e.consume();
				}
				}
		});
		qid.setBounds(67, 124, 80, 21);
		contentPane.add(qid);
		qid.setColumns(10);
		
		JLabel bg = new JLabel("");
		bg.setIcon(new ImageIcon(".\\icon\\zhucebg.jpg"));
		bg.setBounds(0, 0, 436, 296);
		contentPane.add(bg);
		new Thread(new Runnable() {

			@SuppressWarnings("static-access")
			@Override
			public void run() {
				while(true) {
					try {
						Thread.currentThread().sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(icontxt.getText().length()>0)
					icon.setIcon(new ImageIcon(".\\icon\\"+icontxt.getText()+".jpg"));
				}
				
			}}).start();
	}
	
	

}
