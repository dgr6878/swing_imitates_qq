package com.view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.LoginUtil.QQ_loginUtil;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class QQ_Camera extends JFrame {

	private JPanel contentPane;
	QQ_Camera frame=this;
    public static String FACEPATH="D:\\";
    Webcam webcam;
    String id;
  
	public static void main(String[] args) {
	
					new QQ_Camera("123","",0);

	}


	public QQ_Camera(final String id,final String ip,final int port) {
	
		
		this.id=id;
		setTitle("\u4EBA\u8138\u8BC6\u522B");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(430, 110, 655, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("\u62CD\u7167");
		btnNewButton.setFont(new Font("黑体", Font.PLAIN, 16));
		btnNewButton.setBounds(505, 470, 109, 23);
		contentPane.add(btnNewButton);
		
		final JPanel panel = new JPanel();
		panel.setBounds(0, 0, 641, 460);
		contentPane.add(panel);
		
		webcam = Webcam.getDefault(); 
		webcam.setViewSize(WebcamResolution.VGA.getSize());
		WebcamPanel wpanel = new WebcamPanel(webcam);
		wpanel.setFPSDisplayed(true);
		wpanel.setDisplayDebugInfo(true);
		wpanel.setImageSizeDisplayed(true);
		wpanel.setMirrored(true);
		panel.add(wpanel);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				QQ_JIETU t=new QQ_JIETU(id, FACEPATH,frame.getX()+8,frame.getY()+36,frame.getWidth()-20,frame.getHeight()-88);
				JOptionPane.showMessageDialog(panel, "拍照成功！", "提示", JOptionPane.INFORMATION_MESSAGE);
				QQ_loginUtil.facelogin2("123", ip, port);
				webcam.close();//摄像头关闭方法
				dispose();
				
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});
		setVisible(true);
	}
	
	

}
