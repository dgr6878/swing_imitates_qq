package com.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class QQ_people {
	
	JPanel p;
	Socket client;
	String[] allinfo;
	JScrollPane js;
	int panew;
	int y=2;
	String myqq;
	String myicon="";
	JPanel f;
	QQ_del frame2;
	
	
	
	QQ_people(int x,int y,int w,int h,JPanel jp,Socket client,String[] allinfo,String qqid){
		this.client=client;
		this.myqq=qqid;
		this.allinfo=allinfo;
		f = jp;
		panew=w;
		
	    p=new JPanel();
	    p.setOpaque(true);
	    p.setBackground(SystemColor.info);
	    p.setLayout(null);
	    
		 js=new JScrollPane(p);
		 js.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		 js.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		 js.setBounds(x, y, w, h);   
		 js.getVerticalScrollBar().setUnitIncrement(10);

		//鼠标滑轮
		 p.setPreferredSize(new Dimension(w,h));
		 p.validate();
		 f.add(js);
		 for(int i=0;i<allinfo.length;i++) {
			 String one=allinfo[i];
			 String[] oneinfo=getoneinfo(one);
			 huaone(oneinfo[3],oneinfo[2],oneinfo[0],oneinfo);	
			 if(i==0) {
				 myicon=oneinfo[2];//自己头像
			 }
		 }
		 
	}
	
public void removejs() {
		js.remove(p);
		f.remove(js);
		p=null;
		js=null;
	}

public void premoveall() {
	y=2;
	p.removeAll();
}

public void rehuaone(String[] allinfo2) {
	 for(int i=0;i<allinfo2.length;i++) {
		 String one=allinfo2[i];
		 String[] oneinfo=getoneinfo(one);
		 huaone(oneinfo[3],oneinfo[2],oneinfo[0],oneinfo);	
		 if(i==0) {
			 myicon=oneinfo[2];//自己头像
		 }
	 }
}
public String[] getoneinfo(String ss) {
		String[] oneinfo=new String[4];
		int start2=0;
		int end2=0;
		for(int i=0;i<4;i++) {
			
			if(i<1) {
				start2=ss.indexOf("#");
				end2=ss.indexOf("#",start2+1);
				String name=ss.substring(start2+1, end2);
				oneinfo[i]=name;
			}else {
				start2=ss.indexOf("#",end2);
				end2=ss.indexOf("#",start2+1);
				String name=ss.substring(start2+1, end2);
				oneinfo[i]=name;
				
			}	
		}
		return oneinfo;
	}
	
	
public JPanel getJPanel() {
	return p;
}

public void huaone(String name,String icon,String qqid,String[] oneinfo) {
	File dir=new File(".//chatrecord");
	File mydir=new File(".//chatrecord//#"+myqq);
	
	if(!dir.exists()) {
		dir.mkdirs();
	}
	
	if(!mydir.exists()) {
		mydir.mkdirs();
	}
	
	  String recordpath=".//chatrecord//#"+myqq+"//#"+qqid+"#"+myqq+".txt";
	  File myrecord=new File(recordpath);
	if(!myrecord.exists()) {
		try {
			myrecord.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	Mylabel l = new Mylabel(name,qqid,myqq,icon);
	String iconpath=".\\icon\\"+icon+".jpg";
	
	    l.setBackground(SystemColor.info);
	    l.setBounds(5, y, 260, 50);
        l.setIcon(new ImageIcon(iconpath));
	    l.setOpaque(true);
	    l.setForeground(Color.RED);
	    l.setFont(new Font("黑体", Font.PLAIN, 18));
        p.add(l);

        mousejian mjian=new mousejian(l);
        l.addMouseMotionListener(mjian);
        mouseexit mexit =new mouseexit(l,client,oneinfo);
		l.addMouseListener(mexit);
		y=y+l.getHeight()+5;
		p.setPreferredSize(new Dimension(panew,y));
		p.validate();
		
   } 
	






class mousejian implements MouseMotionListener{
	
	JLabel l;
	
	mousejian(JLabel l){
		this.l=l;
	}

	public void mouseMoved(MouseEvent e) {
		l.setBackground(Color.LIGHT_GRAY);
		
		
	}
	
	public void mouseDragged(MouseEvent arg0) {
		
	}
	
}

class mouseexit implements MouseListener{
	
	Mylabel l;
	Socket client;
	QQ_ChatFrame frame=null;
	String[] oneinfo;
	
	
	mouseexit(Mylabel l,Socket t,String[] oneinfo){
		this.oneinfo=oneinfo;
		this.l=l;
		client=t;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int clickcount=e.getClickCount();
		if(clickcount==2&&e.getButton() == MouseEvent.BUTTON1) {//双击事件,并且是左键
			if(frame==null) {
				frame = new QQ_ChatFrame(client,l.getQqid(),l.getMyqq(),l.getName(),myicon,l.geticon());
				frame.setVisible(true);
			}
			
			frame.setVisible(true);
		}
		
		if(clickcount==1&&e.getButton() == MouseEvent.BUTTON3) {
			frame2 = new QQ_del(e.getXOnScreen(), e.getYOnScreen(),oneinfo,myqq,client.getInetAddress().getHostAddress(),client.getPort());
			frame2.setVisible(true);
		}
		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	     l.setBackground(null);
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}


@SuppressWarnings("serial")
class Mylabel extends JLabel{
	private String qqid;
	private String myqq;
	private String name;
	private String icon;



	Mylabel(String name,String qqid,String myqq,String icon){
		super(name);
		this.qqid=qqid;
		this.name=name;
		this.myqq=myqq;
		this.icon=icon;
	}
	
	public String getQqid() {
		return qqid;
	}
	public String getName() {
		return name;
	}
	public String getMyqq() {
		return myqq;
	}
	public String geticon() {
		return icon;
	}
	
	
}

}
