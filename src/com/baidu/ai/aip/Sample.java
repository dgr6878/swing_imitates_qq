package com.baidu.ai.aip;

import java.io.IOException;
import java.util.HashMap;

import org.json.JSONObject;

import com.baidu.ai.aip.utils.FileUtil;
import com.baidu.aip.face.AipFace;
import com.baidu.aip.util.Base64Util;

public class Sample {
    //设置APPID/AK/SK

    public static final String APP_ID = "百度ai申请的appid";
    public static final String API_KEY = "百度ai申请的apikey";
    public static final String SECRET_KEY = "百度ai申请的应用秘钥";

    public static void main(String[] args) {
      
        String path = "C:\\Users\\16631\\Pictures\\Camera Roll\\b.jpg";
        FaceSearch(path);
        //Facezhuce(path,"me3");
        
        
    }
 
    
    
    public static String ToBase64(String path) {
    	
    	byte[] bytes1 = null;
		try {
			bytes1 = FileUtil.readFileByBytes(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Base64Util.encode(bytes1);
    }
    
    
    public static String FaceSearch(String path) {
    	
    	AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("quality_control", "NORMAL");
        options.put("liveness_control", "LOW");
      
        
        String image = ToBase64(path);
        String imageType = "BASE64";
        String groupIdList = "group";
        
        // 人脸搜索
        JSONObject res = client.search(image, imageType, groupIdList, options);
        String tostr=res.toString(2);
        System.out.println(tostr);
        return tostr;

    }
    
    
    public static String Facezhuce(String path,String id) {
    	
    	AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("user_info", "user's info");
        options.put("quality_control", "NORMAL");
        options.put("liveness_control", "LOW");
        
        String image = ToBase64(path);
        String imageType = "BASE64";
        String groupId = "group";
        String userId = id;
        
        // 人脸注册
        JSONObject res = client.addUser(image, imageType, groupId, userId, options);
        String tostr=res.toString(2);
        System.out.println(tostr);
        return tostr;

    }
    
    
    public void sample4(AipFace client) {
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        
        String userId = "user1";
        String groupId = "group1";
        
        // 用户信息查询
        JSONObject res = client.getUser(userId, groupId, options);
        System.out.println(res.toString(2));

    }
}
