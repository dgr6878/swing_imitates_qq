package com.Server;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class QQ_server extends JFrame{

	private static final long serialVersionUID = 1L;
	
	String url = "jdbc:sqlserver://localhost:1433;DatabaseName=QQ_DateBase;";
	Connection conn;
    Statement stmt;
    
    HashMap<String,Socket> socketmap=new HashMap<String,Socket>();

	private JPanel contentPane;
	private JTextField serverip;
	private JTextField localip;
	private JTextField localport;
	private JTextField serverport;
	private JButton BTNserverclose;
	private JScrollPane scrollPane;
    private JScrollPane scrollPane_1;
    jiantin jian=new jiantin();
    String ipaddress2;
    JTextArea info;
    JTextArea chattxt;
    JTextArea fainfo;
    JButton BTNfa;
    JButton BTNserver;
    JButton BTNlianjie;
    
    Socket localsocket;
    List<Socket> cliarr=new ArrayList<Socket>(0);
    List<String> lixianmess=new ArrayList<String>(0);
    Socket server;
    ServerSocket s;
    DataOutputStream out;
    String ipaddress;//对方内网ip
    Socket cli;
    String sport;//对方端口
    int port;
    int port2;
    String dbname="sa";
    String dbpass="123456";
    
    
    
	public static void main(String[] args) {
					QQ_server frame = new QQ_server();
					frame.setVisible(true);	
		}

	
	public QQ_server() {
		setTitle("QQ服务器  V1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);//不可改变大小
		setBounds(100, 100, 460, 449);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblip = new JLabel("\u672C\u673A\u5185\u7F51ip\uFF1A");
		lblip.setFont(new Font("Adobe 黑体 Std R", Font.PLAIN, 14));
		lblip.setBounds(31, 10, 108, 29);
		contentPane.add(lblip);
		
		JLabel lblip_1 = new JLabel("\u8F93\u5165\u670D\u52A1\u5668 ip\uFF1A");
		lblip_1.setFont(new Font("Adobe 黑体 Std R", Font.PLAIN, 14));
		lblip_1.setBounds(31, 89, 108, 29);
		contentPane.add(lblip_1);
		
		localip = new JTextField();
		localip.setColumns(10);
		localip.setBounds(132, 15, 131, 21);
		contentPane.add(localip);
		try {
			localsocket=new Socket("www.baidu.com",80);
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(BTNserver, "网络连接错误，请检查网络！", "帮助", JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		localip.setText(localsocket.getLocalAddress().toString());
		localip.setEditable(false);
		
		serverip = new JTextField();
		serverip.setBounds(132, 94, 131, 21);
		contentPane.add(serverip);
		serverip.setColumns(10);
		serverip.setText("localhost");
		
		JLabel port1jpanel = new JLabel("\u7AEF\u53E3\uFF1A");
		port1jpanel.setFont(new Font("Adobe 黑体 Std R", Font.PLAIN, 14));
		port1jpanel.setBounds(31, 49, 43, 29);
		contentPane.add(port1jpanel);
		
		localport = new JTextField();
		localport.setColumns(10);
		localport.setBounds(132, 55, 50, 21);
		contentPane.add(localport);
		localport.setText("8899");
		
	    BTNserver = new JButton("\u542F\u52A8\u670D\u52A1\u5668");
		BTNserver.setBounds(192, 54, 108, 23);
		contentPane.add(BTNserver);
		BTNserver.addActionListener(jian);
		
		BTNlianjie = new JButton("\u8FDE\u63A5\u670D\u52A1\u5668");
		BTNlianjie.addActionListener(jian);
		BTNlianjie.setBounds(323, 131, 113, 23);
		contentPane.add(BTNlianjie);
		
		JLabel port2 = new JLabel("\u7AEF\u53E3\uFF1A");
		port2.setFont(new Font("Adobe 黑体 Std R", Font.PLAIN, 14));
		port2.setBounds(273, 89, 43, 29);
		contentPane.add(port2);
		
		serverport = new JTextField();
		serverport.setColumns(10);
		serverport.setBounds(326, 94, 50, 21);
		contentPane.add(serverport);
		serverport.setText("1234");
		
		BTNserverclose = new JButton("\u5173\u95ED\u670D\u52A1\u5668");
		BTNserverclose.setEnabled(false);
		BTNserverclose.addActionListener(jian);
		BTNserverclose.setBounds(325, 54, 111, 23);
		contentPane.add(BTNserverclose);
		
		info = new JTextArea();
		info.setBounds(41, 128, 243, 106);
		contentPane.add(info);
		
	    chattxt = new JTextArea();
		chattxt.setLineWrap(true);
		chattxt.setEditable(false);
		chattxt.setBounds(31, 244, 405, 124);
		contentPane.add(chattxt);
		
		fainfo = new JTextArea();
		fainfo.setBounds(47, 375, 253, 21);
		contentPane.add(fainfo);
		
		BTNfa = new JButton("\u53D1 \u9001");
		BTNfa.setBounds(313, 376, 108, 21);
		contentPane.add(BTNfa);
		
		
		scrollPane = new JScrollPane(chattxt);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(41, 244, 395, 124);
		contentPane.add(scrollPane);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		scrollPane_1 = new JScrollPane(info);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(41, 130, 259, 103);
		contentPane.add(scrollPane_1);
		
		JButton btnNewButton = new JButton("\u6E05  \u5C4F");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chattxt.setText("");
				
			}
		});
		btnNewButton.setBounds(324, 211, 97, 23);
		contentPane.add(btnNewButton);
	
		BTNfa.addActionListener(jian);		
		 try {
			conn = DriverManager.getConnection(url, dbname, dbpass);
			  
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(contentPane, "数据库连接错误！！！", "QQ提示", JOptionPane.INFORMATION_MESSAGE);
			System.out.println("数据库连接错误！！！");
		}
       
	}//构造函数结尾
	

class jiantin implements ActionListener{
			
	public boolean relogin(String qq,String mima) {
		     String sql = "select * from QQpersoninfo where qqid="+qq+" and qqpassword="+mima;
	         ResultSet rs;
             try {
            	 stmt = conn.createStatement();
				 rs = stmt.executeQuery(sql);
				 //chattxt.append("正在验证："+qq+"\n");
				  if(rs.next()) {
					  //chattxt.append(qq+"验证成功"+"\n");
					  //System.out.println(qq+"心跳包-验证成功");
					  return true;
				  }
		                    
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return false;	
	}
	public String getpersoninfo(int qq) {
        ResultSet rs;
        String sql = "select * from QQpersoninfo where qqid="+qq;
        String one = "";

        try {
			 rs = stmt.executeQuery(sql);
			   while (rs.next()) {
		              int qqnum=rs.getInt("qqid");
	                 String qqqm=rs.getString("qqqianming");
	                 int qqicon = rs.getInt("qqicon");
	                 String qqname=rs.getString("qqname");
	                 one="$#"+qqnum+"#"+qqqm+"#"+qqicon+"#"+qqname+"#$";
	                 
	                 
	             }
			   
			  // System.out.println(one+getfriend(qq));//所有的初始化信息
			   return one+getfriend(qq);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
       
    
	
}
	
    public String getqun(String qq) {
       ResultSet rs;
       String s = "";
	   String sql=" select s1.qid,qgonggao,qtx,qname,qmember " + 
	   		" from QQquninfo s1,QQqunmember s2 " + 
	   		" where s1.qid=s2.qid and qmember="+qq;
	   
       try {
         
			  rs = stmt.executeQuery(sql);
			   while (rs.next()) {
				      int qid=rs.getInt("qid");
				      int qtx=rs.getInt("qtx");
				      String qname=rs.getString("qname");
				      String qgg=rs.getString("qgonggao");
				      String jiama="$#"+qid+"#"+qgg+"#"+qtx+"#"+qname+"#$";//群号+群名+群头像
	                  s=s+jiama;
			   }
	             return s;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	return null;
	
	
      }
    
    public String getmessage() {
    	String sql="select * from QQmessage";
    	ResultSet rs;
    	String s = "";
    	 try {
             
			 rs = stmt.executeQuery(sql);
			   while (rs.next()) {
				     int mid=rs.getInt("mid");
	                 int micon = rs.getInt("micon");
	                 String mname=rs.getString("mname");
	                 String one="$#"+mid+"#"+"text291"+"#"+micon+"#"+mname+"#$";
	                 s=s+one;
			   }
	             return s;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
    	
    }
    public int doCreate(String zhuce) {
    	  Statement zcst = null;
    	  if(zhuce.indexOf("select")>-1) {
    		  try {
    			  zcst=conn.createStatement();
				ResultSet rs= zcst.executeQuery(zhuce);
				if(rs.next()) {
					return 1;
				}
				else {
					return 0;
				}
					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	  }
    	  
    	  
    	  
			 try {
				 zcst=conn.createStatement();
				// System.out.println(zhuce);//输出注册语句
				return zcst.executeUpdate(zhuce);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
    }
	
	public String getfriend(int qqhao) {
         ResultSet rs;
         String sql = "select * from QQpersoninfo where qqid in (select friend from QQfriend where qqid="+qqhao+")";
         	
         String s = "";
         try {
             // 建立Statement对象
             
			 rs = stmt.executeQuery(sql);
			   while (rs.next()) {
				      int qqnum=rs.getInt("qqid");
	                 String qqqm=rs.getString("qqqianming");
	                 int qqicon = rs.getInt("qqicon");
	                 String qqname=rs.getString("qqname");
	                 String one="$#"+qqnum+"#"+qqqm+"#"+qqicon+"#"+qqname+"#$";
	                 s=s+one;
			   }
	             return s;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	
	}
		
		
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==BTNserver) {
				if(localport.getText().equals("")) {
					JOptionPane.showMessageDialog(BTNserver, "端口为空", "帮助", JOptionPane.INFORMATION_MESSAGE);
                                                    }
				else {
			        port=Integer.parseInt(localport.getText());
				    new Thread(new Runnable() {
					   public void run() {
						 
						 try {
						    s=new ServerSocket(port,3000);
						    while(true) {
							server = s.accept(); 
							info.append(server.getInetAddress()+"    已经连接");
							cliarr.add(server);
							DataInputStream in = new DataInputStream(server.getInputStream());
							DataOutputStream o = new DataOutputStream(server.getOutputStream());
							
							String qq=in.readUTF();
							if(qq.equals("@@@")) {
								String mima=in.readUTF();
								String heamima=in.readUTF();
							   if(relogin(mima,heamima)) {
								    String logininfo="@"+getpersoninfo(Integer.parseInt(mima))+"@"+getqun(mima)+"@"+getmessage()+"@";
									o.writeUTF(logininfo);//好友信息
							   }
							   continue;
							}
							
							int qq2=0;
							
							
							if(!qq.equals("%%%")&&!qq.equals("@@@")) {qq2=Integer.parseInt(qq);}
							
						
							
							
							
							
							if(socketmap.containsKey(qq)) {//单一登录控制
								o.writeUTF("onlyone");
								server.close();
								continue;
							}////////////////////////////////////////////
							String mima=in.readUTF();
							if(qq.equals("%%%")) {//注册识别码
								String sql=mima;
								if(doCreate(sql)>0) {
									o.writeUTF("成功");
								}else {
									o.writeUTF("失败");
								}
							    continue;
							}
							
							if(relogin(qq,mima)) {
								o.writeUTF("true");
								socketmap.put(qq, server);
								String logininfo="@"+getpersoninfo(qq2)+"@"+getqun(qq)+"@"+getmessage()+"@";
								o.writeUTF(logininfo);//好友信息
								new printinfo(qq,server);
								
							}else {
								o.writeUTF("false");
								server.close();
							}
						
						    }
				                        
						} catch (IOException e1) {
							e1.printStackTrace();
						}}}).start();

						   			   
			   info.append("端口："+port+"  等待连接...."+"\n");
			   BTNserver.setText("已经启动");
			   BTNserver.setBackground(Color.red);
				  }
				
			}else if(e.getSource()==BTNlianjie) {
				ipaddress2=serverip.getText();
				port2=Integer.parseInt(serverport.getText());
				if(ipaddress2.equals("")||serverport.getText().equals("")) {
					JOptionPane.showMessageDialog(BTNserver, "端口或者IP有误！", "帮助", JOptionPane.INFORMATION_MESSAGE);
				}else {
					new Thread(new Runnable() {
						public void run() {
							try {
								cli=new Socket(ipaddress2,port2);
							} catch (UnknownHostException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							info.append("正在连接："+ipaddress2+"  端口："+port2+"\n");
							if(cli.isConnected()) 
							     info.append("连接成功  ^-^\n");
						}
					}).start();
				}
			}
			else if(e.getSource()==BTNserverclose) {
				
				try {
					info.append("端口："+port+"  本地服务器关闭"+"\n");
					BTNserver.setText("启动服务器");
					BTNserver.setBackground(Color.lightGray);
					s.close();	
				}catch (Exception e1) {
					e1.printStackTrace();
				                      }
				                                   }
			else if(e.getSource()==BTNfa) {
				for(int i=0;i<cliarr.size();i++) {
				
				try {
					String tomess=fainfo.getText()+"\n";
					out = new DataOutputStream(cliarr.get(i).getOutputStream());
					out.writeUTF(tomess);
					chattxt.append("本机："+tomess);
					fainfo.setText("");
					
				
				
				}catch (IOException e1) {
					e1.printStackTrace();
				}
				
				
				}
			
					
				}
	  }
   }



class printinfo extends Thread{
	
	DataInputStream in;
	Socket cli;
	String qq;
	printinfo(String qq,Socket cli){
		this.qq=qq;
		this.cli=cli;
		this.start();
	}
	public List<String> getqunmem(String qunid) {
	
	         ResultSet rs;
	         String sql = "select * from QQqunmember where qid ="+qunid;
	         List<String> a=new ArrayList<String>(0);
	         try {
	             
				 rs = stmt.executeQuery(sql);
				   while (rs.next()) {
					      int qmember=rs.getInt("qmember");
		                  a.add(""+qmember);
				   }
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return a;
		
		
	}
    private String machine(String quesiton) throws IOException {
        //接入机器人，输入问题
        String APIKEY = "d5b619ad290c44f192c14bd3c9ecd097";
        String INFO = URLEncoder.encode(quesiton, "utf-8");//这里可以输入问题
        String getURL = "http://www.tuling123.com/openapi/api?key=" + APIKEY + "&info=" + INFO;
        URL getUrl = new URL(getURL);
        HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
        connection.connect();

        // 取得输入流，并使用Reader读取
        BufferedReader reader = new BufferedReader(new InputStreamReader( connection.getInputStream(), "utf-8"));
        StringBuffer sb = new StringBuffer();
        String line = "";
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        // 断开连接
        connection.disconnect();
        String[] ss = new String[10];
        String s = sb.toString();
        String answer;
        ss = s.split(":");
        answer = ss[ss.length-1];
        answer = answer.substring(1,answer.length()-2);
        return answer;
    }
	

	
	
	public boolean zhuanfa(String ss) {//转发器
		String[] info=new String[3];
		int start=0;
		int end=0;
		int zhong=0;
		start=ss.indexOf("#");
		zhong=ss.indexOf("#",start+1);
		end=ss.indexOf("#",zhong+1);
		info[0]=ss.substring(start+1, zhong);//from   no #
		info[1]=ss.substring(zhong+1,end);//to   no #
		info[2]=ss.substring(end+1);//mess  no #
		Socket to=socketmap.get(info[1]);
		try {
			if(info[1].equals("8888")) {//聊天机器人账号
			    DataOutputStream out = new DataOutputStream(socketmap.get(info[0]).getOutputStream());
			    String ans=machine(info[2]);
			    out.writeUTF("#"+info[1]+"#"+info[0]+"#"+ans);
			    System.out.println("机器人："+ans);
			    }
			
			if(to!=null) {
			    DataOutputStream out = new DataOutputStream(to.getOutputStream());
			    out.writeUTF(ss);
			    System.out.println("已发送："+ss);
			    }
			else {
				List<String> member=getqunmem(info[1]);
				Iterator<String> it=member.iterator();
				while(it.hasNext()) {
					String qmem=(String) it.next();
					Socket to2=socketmap.get(qmem);
					if(to2!=null) {
						
					    DataOutputStream out2 = new DataOutputStream(to2.getOutputStream());
					    String messzhuan="#"+info[1]+"#"+qmem+"#"+info[2];
					   
					    if(!qmem.equals(info[0]))
					    	 out2.writeUTF(messzhuan);
					    System.out.println("已发送："+messzhuan);
					        // return true;
					    }
					
				}
				
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
		
	}
	public void run() {
		try {
			in = new DataInputStream(cli.getInputStream());
			
			while(true) {
				if(cli.isConnected()) {
					String mess=in.readUTF();
					chattxt.append(cli.getInetAddress()+"说:"+mess+"\n");	
					zhuanfa(mess);
					
				
				}
				else {cli.close(); }
				
				}
		} catch (SocketException e) {
			try {
				cli.close();
				socketmap.remove(qq);
				chattxt.append("账号ID:"+qq+"  客户退出\n");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		
	}
}


}


