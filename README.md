# swing模仿QQ

#### 介绍
使用java  swing模仿qq,没有任何框架，仅仅java SE基础知识。实现了人脸识别的登录（去百度ai注册一个账号）

#### 软件架构
1. 基于jdk 10开发，1.8也可以运行，区别就是10运行的界面大一些，1.8的小一些。
2. 数据库 SQL Server2008 R2 (换成mysql也可以，驱动改一下就行)
3. 可以使用exe4j和inno  setup实现exe安装运行，我打包了一个exe文件（根目录的mysetup.exe），可以直接安装。
4. 聊天记录保存在本地txt文件，chatrecord文件夹，按照qq号码区分聊天记录。
5. 有一些多线程的简单使用，socket进行通讯，公网部署，只需要吧运行的数据库和服务端在服务器运行起来即可，客户端点击右上角的设置改一下服务端的地址即可。


#### 安装教程

1.  导入SQL文件
2.  eclipse导入这个项目
3.  点击com.server包下面的QQ_Server.java启动服务端
4.  点击com.view的QQ_loginstart.java启动客户端

#### 使用说明

1.  支持单聊，群聊，修改资料，添加好友，挂机等。

#### 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/155806_b96839de_6562129.png "image-20200820154517145.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/155816_c31aff4d_6562129.png "image-20200820154533417.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/155828_44222dd7_6562129.png "image-20200820154543897.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/160447_16d74727_6562129.png "image-20200820154607445.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/160639_769b6c6c_6562129.png "image-20200820154622015.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0820/155740_e1d245fe_6562129.png "image-20200820154628890.png")
